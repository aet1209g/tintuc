<?php

namespace common\models; 

use Yii; 

/** 
 * This is the model class for table "advertise". 
 * 
 * @property integer $id
 * @property string $details
 * @property string $advertise_key
 * @property string $image
 * @property integer $expire
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image_default
 */ 
class Advertise extends \yii\db\ActiveRecord
{ 
    /** 
     * @inheritdoc 
     */ 
    public static function tableName() 
    { 
        return 'advertise'; 
    } 

    /** 
     * @inheritdoc 
     */ 
    public function rules() 
    { 
        return [
            [['details', 'advertise_key', 'expire'], 'required'],
            [['expire', 'created_at', 'updated_at'], 'integer'],
            [['details', 'advertise_key','link_advertise'], 'string', 'max' => 255],
            [['image', 'image_default'], 'string', 'max' => 200],
        ]; 
    } 

    /** 
     * @inheritdoc 
     */ 
    public function attributeLabels() 
    { 
        return [ 
            'id' => 'ID',
            'details' => 'Mô tả quảng cáo',
            'advertise_key' => 'Key',
            'image' => 'Banner',
            'expire' => 'Ngày hết hạn',
            'updated_at' => 'Cập nhật lúc',
            'image_default' => 'Ảnh mặc định',
            'link_advertise' => 'Link quảng cáo',
        ]; 
    } 
    public static function getAds(){
        $all = self::find()->all();
        $arr = array();
        foreach($all as $ads){
            if($ads->expire > time()){
                $array = array(
                    'image' => $ads->image,
                    'link'=> $ads->details
                );
            }else{
                $array = array(
                    'image' => $ads->image_default,
                    'link'=> "javascript:void(0)"
                );
            }
            
            $arr[$ads->advertise_key] = (object) $array;
        }
        return (object) $arr;
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($insert)
                $this->created_at = time();
            else
                $this->updated_at = time();
            //$this->pasword = AES::encrypt(123456);
            return true;
        } else {
            return false;
        }
    }
    // public function getImageurl()
    // {
    //     return '/files/img/advertise/'.$model->image.'.img';
    // }
}