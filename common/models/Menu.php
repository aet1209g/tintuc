<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $menu_id
 * @property string $menu_title
 * @property string $menu_link
 * @property string $menu_css
 * @property integer $menu_parent_id
 */
class Menu extends \yii\db\ActiveRecord
{   
    public $childs;
    public $highlight;
    public $posts;
    public $categories;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_title'], 'required'],
            [['menu_id', 'menu_parent_id'], 'integer'],
            [['menu_title', 'menu_css'], 'string', 'max' => 100],
            [['news_id'], 'string', 'max' => 50],
            [['menu_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => 'Menu ID',
            'menu_title' => 'Tên menu',
            'menu_link' => 'Link',
            'menu_css' => 'Css trong menu',
            'menu_parent_id' => 'Menu cha',
        ];
    }
    public static function getList($excludeId = 0)
    {
        $list = [];
        if (empty($excludeId)) {
            $query = self::find()->orderBy('menu_title');
            $models = $query->all();
            $list[0] = "ROOT - gốc";
            foreach ($models as $model) {
                $list[$model->menu_id] = $model->menu_title;
            }
        }else{
            $models = MenuNews::find(['menu_id' => $excludeId])->one();
            return $models->menu_id;
        }

        return $list;
    }

    public static function getMenu()
    {
        $alls = self::find()->where('menu_parent_id = 0')->all();
        foreach($alls as $menu){
            $menu->childs = self::find()->where(['menu_parent_id' => $menu->menu_id])->all();
        }
        return $alls;
    }

    public static function getParentMenu()
    {
        $alls = self::find()->where('menu_parent_id = 0')->all();
        $arr = [];
        foreach ($alls as $k => $v) {
            $arr[$v->menu_id] = $v->menu_title;
        }
        return $arr;
    }
    public static function getSuggestMenu($id){
        $menu = self::find()->where("menu_id = ".$id)->one();
        if($menu->menu_parent_id > 0)
            $menu = self::find()->where("menu_id = ".$menu->menu_parent_id)->one();
        $menu->childs = self::find()
                        ->where(['menu_parent_id' => $menu->menu_id])
                        ->andWhere(['<>', 'menu_id', $id])
                        ->all();
        foreach($menu->childs as $mn){
            $cate = [];
            $mn->categories = $mn->getCateMenu()->all();
            foreach($mn->categories as $ca){
                $cate[] = $ca->cate_id;
            }
            $mn->posts = News::find()
                            ->leftJoin('cate_news', 'cate_news.news_id = news.news_id')
                            ->where(["news_status" => News::status_published])
                            ->andWhere(['cate_news.cate_id' => $cate ])
                            ->orderBy('news.published_at DESC')
                            ->limit(10)
                            ->all();
        }
        return $menu->childs;
    }
    public static function getMenuWithNews($limit = 3){
        $menus = self::getMenu();
        foreach($menus as $menu){
            $cate = [];
            $menu->highlight = News::find()
                            ->where(["news_status" => News::status_published])
                            ->andWhere(['news_id' => explode(",", $menu->news_id) ])
                            ->orderBy([new \yii\db\Expression('FIELD (news_id, '.$menu->news_id.')')])
                            ->limit(1)
                            ->one();
            $menu->categories = $menu->getCateMenu()->all();
            foreach($menu->categories as $ca){
                $cate[] = $ca->cate_id;
            }
            $menu->posts = News::find()
                            ->leftJoin('cate_news', 'cate_news.news_id = news.news_id')
                            ->where(["news_status" => News::status_published])
                            ->andWhere(['not in','news.news_id',explode(",", $menu->news_id)])
                            ->andWhere(['cate_news.cate_id' => $cate ])
                            ->orderBy('news.published_at DESC')
                            ->limit($limit)
                            ->all();
        }
        return $menus;
    }

    public function getCateMenu()
    {
        return $this->hasMany(CateMenu::className(), ['menu_id' => 'menu_id']);
    }
}