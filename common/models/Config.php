<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $config_id
 * @property string $config_key
 * @property string $config_value
 * @property string $title
 * @property string $desciprtion
 * @property string $keyword
 * @property string $title_social
 * @property string $description_social
 * @property string $image_social
 * @property string $facebook_page
 * @property string $google_page
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_key', 'config_value', 'title', 'desciprtion', 'keyword', 'title_social', 'description_social', 'image_social', 'facebook_page', 'google_page','email'], 'required'],
            [['config_key', 'config_value', 'address'], 'string', 'max' => 200],
            
            [['title', 'desciprtion', 'keyword','email', 'title_social', 'description_social', 'image_social', 'facebook_page', 'google_page','footer_header', 'footer_content'], 'string', 'max' => 255],
            [['config_number'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'config_id' => 'Config ID',
            'config_key' => 'Từ khoá chính',
            'config_value' => 'Chỉnh sửa giá trị',
            'title' => 'Tiêu đề web',
            'desciprtion' => 'Miêu tả trang',
            'keyword' => 'Khoá chính',
            'title_social' => 'Hiển thị thông tin trên web',
            'description_social' => 'Miêu tả trên web',
            'image_social' => 'Ảnh trên web',
            'facebook_page' => 'Trang trên facebook',
            'google_page' => 'Trang trên google',
            'email' => 'email',
            'address' => 'Địa chỉ',
            'config_number' => 'số điện thoại',
            'footer_header' => 'Tiêu đề chân trang',
            'footer_content' => 'Nội dung chân trang',
        ];
    }
}
