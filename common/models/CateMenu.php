<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cate_menu".
 *
 * @property integer $menu_id
 * @property integer $cate_id
 */
class CateMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cate_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'cate_id'], 'required'],
            [['menu_id', 'cate_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'menu_id' => 'Menu ID',
            'cate_id' => 'Cate ID',
        ];
    }
}
