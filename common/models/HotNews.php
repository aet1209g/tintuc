<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hot_news".
 *
 * @property integer $hotest
 * @property string $others
 */
class HotNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hot_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotest'], 'required'],
            [['hotest'], 'integer'],
            [['others', 'special_news'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hotest' => 'Tin nóng nhất',
            'others' => 'Tin nổi bật (hàng ngang)',
        ];
    }
}
