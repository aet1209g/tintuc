<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property string $username
 * @property string $created_at
 * @property integer $news_id
 * @property string $is_create
 * @property integer $user_id
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'created_at', 'news_id', 'is_create', 'user_id'], 'required'],
            [['created_at', 'is_create'], 'safe'],
            [['news_id', 'user_id'], 'integer'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'created_at' => 'Created At',
            'news_id' => 'News ID',
            'is_create' => 'Is Create',
            'user_id' => 'User ID',
        ];
    }
}
