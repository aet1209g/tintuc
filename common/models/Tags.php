<?php

namespace common\models;

use Yii;
use common\helpers\Helper;
/**
 * This is the model class for table "tags".
 *
 * @property integer $tag_id
 * @property string $tag_title
 * @property string $tag_slug
 * @property integer $tag_status
 * @property string $created_by
 * @property integer $created_at
 */
class Tags extends \yii\db\ActiveRecord
{
    const status_deactive = 0;
    const status_active = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_title'], 'required'],
            [['tag_status', 'created_at', 'updated_at'], 'integer'],
            [['tag_title', 'tag_slug'], 'string', 'max' => 200],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => 'ID',
            'tag_title' => 'Chủ đề',
            'tag_slug' => 'Alias',
            'tag_status' => 'Trạng thái',
            'created_by' => 'Tạo bởi',
            'created_at' => 'Ngày tạo',
        ];
    }


    public function getListStatus(){
        return [
            0 => "Ẩn",
            1 => "Hiển thị"
        ];
    }

    public function getStatusByID($status_id){
        $status = self::getListStatus();
        foreach ($status as $k => $v) {
            if($status_id == $k)
                return $v;
        }

        return false;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->tag_slug = Helper::alias($this->tag_title);

            if($insert){
                $this->created_at = time();
                $this->created_by = Yii::$app->user->identity->username;
            }
            else{
                $this->updated_at = time();
                $this->updated_by = Yii::$app->user->identity->username;
            }

            
            return true;
        } else {
            return false;
        }
    }
}
