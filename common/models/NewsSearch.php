<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\News;

/**
 * NewsSearch represents the model behind the search form about `common\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'news_viewed', 'updated_at', 'created_at', 'published_at', 'news_status'], 'integer'],
            [['news_title', 'news_slug', 'news_content', 'news_image', 'news_keywords', 'news_description', 'created_by', 'published_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        $this->load($params);


        if($this->published_at){
            $date = explode(' - ', $this->published_at);
            $query->andFilterWhere(['<=', 'published_at', strtotime($date[1])]);
            $query->andFilterWhere(['>=', 'published_at', strtotime($date[0])]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'news_id' => $this->news_id,
            'news_viewed' => $this->news_viewed,
            'created_at' => $this->created_at,
            'news_status' => $this->news_status,
        ]);

        $query->andFilterWhere(['like', 'news_title', $this->news_title])
            ->andFilterWhere(['like', 'news_slug', $this->news_slug])
            ->andFilterWhere(['like', 'news_content', $this->news_content])
            ->andFilterWhere(['like', 'news_keywords', $this->news_keywords])
            ->andFilterWhere(['like', 'news_description', $this->news_description])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'published_by', $this->published_by]);

        if(Yii::$app->user->identity->role_id == User::role_acceptor)
            $query->andWhere("
                (created_by NOT IN (SELECT username FROM user WHERE role_id IN (".implode(",", [User::role_publisher, User::role_superadmin]).")))
                AND
                news_status <> ".News::status_default."
                OR
                (news_status = ".News::status_default." AND created_by = '".Yii::$app->user->identity->username."')
            ");

        if(Yii::$app->user->identity->role_id == User::role_editor)
            $query->andFilterWhere(['=', 'created_by', Yii::$app->user->identity->username]);

        if(Yii::$app->user->identity->role_id == User::role_publisher)
            $query->andWhere("
                news_status <> ".News::status_default."
                OR
                (news_status = ".News::status_default." AND created_by = '".Yii::$app->user->identity->username."')
            ");

        // if(Yii::$app->user->identity->role_id == User::role_publisher)
        //     $query->andWhere("
        //         news_status = ".News::status_published."
        //         OR
        //         news_status = ".News::status_rejected."
        //         OR
        //         news_status <> ".News::status_default."
        //         OR
        //         (news_status = ".News::status_default." AND created_by = '".Yii::$app->user->identity->username."')
        //     ");

        return $dataProvider;
    }
}
