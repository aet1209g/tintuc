<?php

namespace common\models;

use Yii;
use common\helpers\Helper;

/**
 * This is the model class for table "categories".
 *
 * @property integer $cate_id
 * @property string $cate_title
 * @property string $cate_slug
 * @property string $cate_keywords
 * @property string $cate_description
 * @property string $cate_robots
 * @property integer $cate_total_news
 * @property integer $add_user_id
 * @property string $add_username
 * @property string $add_datetime
 * @property integer $cate_slug_id
 */
class Categories extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_title'], 'required'],
            [['cate_total_news', 'add_user_id', 'cate_status'], 'integer'],
            [['add_datetime'], 'safe'],
            [['cate_title', 'cate_slug', 'cate_keywords'], 'string', 'max' => 200],
            [['cate_description'], 'string', 'max' => 255],
            [['add_username'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cate_id' => 'ID',
            'cate_title' => 'Tên chuyên mục',
            'cate_status' => 'Trạng thái',
            'cate_slug' => 'Alias',
            'cate_keywords' => 'Google: Từ khóa tìm kiếm chuyên mục',
            'cate_description' => 'Google: Diễn tả chuyên mục',
            'cate_robots' => 'Cate Robots',
            'cate_total_news' => 'Cate Total News',
            'add_user_id' => 'Add User ID',
            'add_username' => 'Add Username',
            'add_datetime' => 'Add Datetime',
            'cate_slug_id' => 'Cate Slug ID',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->cate_slug == "")
                $this->cate_slug = Helper::alias($this->cate_title);
            //$this->pasword = AES::encrypt(123456);
            return true;
        } else {
            return false;
        }
    }

    public function getListStatus(){
        return [
            0 => "Không kích hoạt",
            1 => "Kích hoạt"
        ];
    }

    public function getStatusByID($status_id){
        $status = self::getListStatus();
        foreach ($status as $k => $v) {
            if($status_id == $k)
                return $v;
        }

        return false;
    }

    public static function getList($excludeId = 0)
    {
        $list = [];
        if (empty($excludeId)) {
            $query = self::find()->where(['cate_status' => Categories::STATUS_ACTIVE])->orderBy('cate_title');
            $models = $query->all();
            foreach ($models as $model) {
                $list[$model->cate_id] = $model->cate_title;
            }
        }else{
            $models = CateNews::find()->where(['news_id' => $excludeId])->asArray()->all();
            foreach ($models as $model) {
                $list[] = $model['cate_id'];
            }
        }

        return $list;
    }

    public static function getListWMenu($excludeId = 0)
    {
        $list = [];
        if (empty($excludeId)) {
            $query = self::find()->where(['cate_status' => Categories::STATUS_ACTIVE])->orderBy('cate_title');
            $models = $query->all();
            foreach ($models as $model) {
                $list[$model->cate_id] = $model->cate_title;
            }
        }else{
            $models = CateMenu::find()->where(['menu_id' => $excludeId])->asArray()->all();
            foreach ($models as $model) {
                $list[] = $model['cate_id'];
            }
        }

        return $list;
    }
}
