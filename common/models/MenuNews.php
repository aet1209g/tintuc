<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cate_news".
 *
 * @property integer $news_id
 * @property integer $cate_id
 * @property integer $updated_at
 * @property integer $updated_by
 */
class CateNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'menu_id'], 'required'],
            [['news_id', 'menu_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'new_id' => 'News ID',
            'menu_id' => 'Menu ID'
        ];
    }
}
