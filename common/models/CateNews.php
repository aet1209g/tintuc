<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cate_news".
 *
 * @property integer $news_id
 * @property integer $cate_id
 * @property integer $updated_at
 * @property integer $updated_by
 */
class CateNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cate_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'cate_id'], 'required'],
            [['news_id', 'cate_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'cate_id' => 'Cate ID'
        ];
    }
}
