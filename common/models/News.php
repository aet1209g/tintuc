<?php

namespace common\models;

use Yii;
use common\helpers\Helper;

/**
 * This is the model class for table "news".
 *
 * @property integer $news_id
 * @property string $news_title
 * @property string $news_slug
 * @property string $news_content
 * @property string $new_image
 * @property string $news_keywords
 * @property string $news_description
 * @property string $news_robots
 * @property integer $news_viewed
 * @property string $news_tagid
 * @property string $news_tagtitle
 * @property integer $cate_id
 * @property string $cate_title
 * @property string $cate_slug
 * @property integer $add_userid
 * @property string $add_username
 * @property string $add_datetime
 * @property integer $add_date_int
 * @property string $public_datetime
 * @property integer $public_datetime_int
 * @property integer $news_status
 */
class News extends \yii\db\ActiveRecord
{
    const status_accept = 1;
    const status_cancel = 2;
    const status_default = 0;
    const status_pending = 3;
    const status_published = 4;
    const status_rejected = 5;
    public $other;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_title', 'news_content', 'news_description', 'news_status'], 'required'],
            [['news_content'], 'string'],
            [['news_viewed', 'created_at', 'accepted_at', 'published_at', 'updated_at', 'news_status', 'news_id', 'money', 'paid_at', 'main_cate_id'], 'integer'],
            [['news_title', 'news_slug'], 'string', 'max' => 200],
            [['author', 'paid_by', 'accepted_by'], 'string', 'max' => 100],
            [['other_news'], 'safe'],
            [['news_image', 'news_keywords', 'news_description', 'source'], 'string', 'max' => 255],
            [['created_by', 'updated_by', 'published_by'], 'string', 'max' => 50],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->news_slug == "")
                $this->news_slug = Helper::alias($this->news_title);
            if(Yii::$app->user->identity){
                if($insert)
                    $this->created_by = Yii::$app->user->identity->username;
                else
                    $this->updated_by = Yii::$app->user->identity->username;
            }
            return true;
        } else {
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'ID tin tức',
            'news_title' => 'Tiêu đề',
            'news_slug' => 'Alias',
            'news_content' => 'Nội dung',
            'news_viewed' => 'Lượt xem',
            'money' => 'Nhuận bút',
            'news_image' => 'Ảnh đại diện',
            'news_keywords' => 'Google: từ khóa',
            'news_description' => 'Google: Tóm tắt bài viết',
            'created_by' => 'Người viết',
            'created_at' => 'Ngày tạo',
            'published_at' => 'Ngày xuất bản',
            'published_by' => 'Người xuất bản',
            'updated_by' => 'Người chỉnh sửa',
            'updated_at' => 'Chỉnh sửa lần cuối',
            'news_status' => 'Trạng thái',
            'news_status' => 'Trạng thái',
            'author' => 'Tác giả',
            'source' => 'Nguồn',
            'other_news' => 'Tin liên quan',
        ];
    }

    public function getListStatus(){
        return [
            0 => "Đang biên tập",
            3 => "Chưa được duyệt",
            1 => "Đã duyệt",
            2 => "Không được duyệt",
            4 => "Đã đăng",
            5 => "Bị gỡ",
        ];
    }
    
    public function getCateNews()
    {
        return $this->hasMany(CateNews::className(), ['news_id' => 'news_id']);
    }
    
    public function getTags()
    {
        return $this->hasMany(TagNews::className(), ['news_id' => 'news_id']);
    }

    public function getStatusByID($status_id){
        $status = self::getListStatus();
        foreach ($status as $k => $v) {
            if($status_id == $k)
                return $v;
        }

        return false;
    }

    public static function getListTitle($excludeId = 0)
    {
        $list = [];
        if (!empty($excludeId)) {
            $models = News::find()->where('news_id IN ('. $excludeId.")")->asArray()->all();
            foreach ($models as $model) {
                $list[] = $model['news_title'];
            }
        }

        return $list;
    }
}