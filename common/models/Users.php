<?php

namespace common\models;

use Yii;
use common\helpers\AES;
/**
 * This is the model class for table "users".
 *
 * @property integer $user_id
 * @property string $user_name
 * @property integer $user_status
 * @property integer $user_level
 * @property string $user_password
 * @property string $user_email
 * @property string $user_avatar
 * @property integer $created_at
 * @property integer $updated_at
 */
class Users extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'user_status', 'user_level', 'user_password', 'user_email', 'user_avatar', 'created_at', 'updated_at'], 'required'],
            [['user_status', 'user_level', 'created_at', 'updated_at'], 'integer'],
            [['user_name'], 'string', 'max' => 50],
            [['user_password'], 'string', 'max' => 100],
            [['user_email', 'user_avatar'], 'string', 'max' => 200],
            ['user_password', 'required'],
            ['user_password', 'string', 'min' => 6],
            ['user_password', 'required'],
            //['user_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'user_name' => 'User Name',
            'user_status' => 'User Status',
            'user_level' => 'User Level',
            'user_password' => 'User Password',
            'user_email' => 'User Email',
            'user_avatar' => 'User Avatar',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id, 'user_status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['user_name' => $username, 'user_status' => self::STATUS_ACTIVE]);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->user_password == "")
                die('Password is invalid!');
            else
                $this->user_password = AES::encrypt($this->user_password);
            return true;
        } else {
            return false;
        }
    }
}
