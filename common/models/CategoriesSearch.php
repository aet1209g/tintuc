<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Categories;

/**
 * CategoriesSearch represents the model behind the search form about `common\models\Categories`.
 */
class CategoriesSearch extends Categories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_id', 'cate_status', 'cate_total_news', 'add_user_id'], 'integer'],
            [['cate_title', 'cate_slug', 'cate_keywords', 'cate_description', 'add_username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cate_id' => $this->cate_id,
            'cate_total_news' => $this->cate_total_news,
            'add_user_id' => $this->add_user_id,
            'cate_status' => $this->cate_status,
            'add_datetime' => $this->add_datetime,
        ]);

        $query->andFilterWhere(['like', 'cate_title', $this->cate_title])
            ->andFilterWhere(['like', 'cate_slug', $this->cate_slug])
            ->andFilterWhere(['like', 'cate_keywords', $this->cate_keywords])
            ->andFilterWhere(['like', 'cate_description', $this->cate_description])
            ->andFilterWhere(['like', 'add_username', $this->add_username]);

        return $dataProvider;
    }
}
