<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Config;

/**
 * ConfigSearch represents the model behind the search form about `common\models\Config`.
 */
class ConfigSearch extends Config
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_id'], 'integer'],
            [['config_key', 'config_value', 'title', 'desciprtion', 'keyword', 'title_social', 'description_social', 'image_social', 'facebook_page', 'google_page'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Config::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'config_id' => $this->config_id,
        ]);

        $query->andFilterWhere(['like', 'config_key', $this->config_key])
            ->andFilterWhere(['like', 'config_value', $this->config_value])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'desciprtion', $this->desciprtion])
            ->andFilterWhere(['like', 'keyword', $this->keyword])
            ->andFilterWhere(['like', 'title_social', $this->title_social])
            ->andFilterWhere(['like', 'description_social', $this->description_social])
            ->andFilterWhere(['like', 'image_social', $this->image_social])
            ->andFilterWhere(['like', 'facebook_page', $this->facebook_page])
            ->andFilterWhere(['like', 'google_page', $this->google_page])
            ->andFilterWhere(['like', 'footer_header', $this->footer_header])
            ->andFilterWhere(['like', 'footer_content', $this->footer_content]);

        return $dataProvider;
    }
}
