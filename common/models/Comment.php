<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $comment_id
 * @property string $comment_content
 * @property integer $news_id
 * @property integer $comment_status
 * @property integer $add_user_id
 * @property string $add_username
 * @property string $add_datetime
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_content', 'news_id', 'comment_status', 'add_user_id', 'add_username', 'add_datetime'], 'required'],
            [['news_id', 'comment_status', 'add_user_id'], 'integer'],
            [['add_datetime'], 'safe'],
            [['comment_content'], 'string', 'max' => 200],
            [['add_username'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Comment ID',
            'comment_content' => 'Comment Content',
            'news_id' => 'News ID',
            'comment_status' => 'Comment Status',
            'add_user_id' => 'Add User ID',
            'add_username' => 'Add Username',
            'add_datetime' => 'Add Datetime',
        ];
    }
}
