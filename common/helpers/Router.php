<?php

namespace common\helpers;

use yii\helpers\Url;

class Router {

    public static function build($route, $vars = array()) {
        $array = array();
        switch ($route) {

            case 'chitietchuongtrinh/index':
                $array[] = 'chuong-trinh';
                $array[] = $vars['alias'];
                $array[] = $vars['id'] . '.html';
                break;

            case 'chitietlich/index':
                $array[] = 'lich-phat-song-kenh';
                $array[] = $vars['alias'];
                $array[] = $vars['id'] . '.html';
                break;

            case 'playing/index':
                $array[] = 'dang-phat-song.html';
                break;


            case 'site/dai':
                $array[] = 'lich-phat-song-'.$vars['alias'];
                $array[] = $vars['did'] . '.html';
                //print_r($array);die;
                break;
            // phim
            case 'hot/index':
                $array[] = 'chuong-trinh-hot.html';
                break;

            case 'category/index':
                $array[] = 'the-loai';
                $array[] = $vars['alias'];
                $array[] = $vars['id'] . '.html';
                break;

            case 'news/index':
                $array[] = 'tin-tuc-' . $vars['page'] . '.html';
                break;

            case 'news/detail':
                $array[] = 'tin-tuc';
                $array[] = $vars['alias'];
                $array[] = $vars['id'] . '.html';
                break;

            case 'dai/index':
                $array[] = 'lich-phat-song-cac-kenh';
                $array[] = $vars['alias'];
                $array[] = $vars['did'] . '.html';
                break;
            
            case 'tv/index':
                $array[] = 'tv/kenh';
                $array[] = $vars['alias'];
                $array[] = $vars['kid'] . '.html';
                break;
            

            //=======xemtructuyen=======
            case 'tv/chitiet':
                $array[] = 'clip/'.$vars['alias'];
                $array[] = $vars['id'] . '.html';
                //print_r($array);die;
                break;
            /*case 'tv/timkiem':
                $array[] = $route;
                print_r($route);die;
                break;*/
            //=======xemtructuyen=======
            default:
                $array[] = $route;
                break;
        }
        return Url::base() . '/' . Url::to(implode('-', $array));
    }

}
