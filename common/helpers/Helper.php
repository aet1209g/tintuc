<?php

namespace common\helpers;
use yii\helpers\Url;
use common\models\News;
class Helper
{
	public static function alias($title, $replace = '-')
	{
		$title = preg_replace("/\p{Han}+/u", '', $title);
		$title = str_replace(".", '', $title);
		/*
		 * Replace with "-" Change it if you want
		 */
		$replacement = $replace;
		$map = array();
		$quotedReplacement = preg_quote($replacement, '/');

		$default = array(
			'/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
			'/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
			'/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
			'/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø|Ø/' => 'o',
			'/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
			'/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/' => 'y',
			'/đ|Đ/' => 'd',
			'/ç/' => 'c',
			'/ñ/' => 'n',
			'/ä|æ/' => 'ae',
			'/ö/' => 'oe',
			'/ü/' => 'u',
			'/Ä/' => 'Ae',
			'/Ü/' => 'U',
			'/Ö/' => 'Oe',
			"/'|!|•/" => '',
			'/ß/' => 'ss',
			'/[’‘‹›‚]/u'    =>   '', // Literally a single quote
        	'/[“”«»„]/u'    =>   '', // Double quote
			'/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
			'/\\s+/' => $replacement,
			sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => ''
		);
		// Some URL was encode, decode first
		$title = htmlentities(str_replace('µ', 'u', $title));
		$title = html_entity_decode($title);
		$title = urldecode($title);

		$map = array_merge($map, $default);
		$title = mb_strtolower(preg_replace(array_keys($map), array_values($map), $title), "UTF-8");
		$title = preg_replace('/[^\00-\255]+/u', '', $title);
		return mb_strtolower(preg_replace(array_keys($map), array_values($map), $title), "UTF-8");
		// ---------------------------------o
	}
    public static function getCategoryUrl($slug){
        if($slug =='/') return '/';
        return Url::to(["site/danh-muc", "slug" => $slug],true);
    }
    public static function getPostUrl($slug){
        return Url::to(["site/bai-viet","slug" => $slug], true);
    }
    public static function getSearchUrl($keyword){
        return Url::to(["site/tim-kiem","keyword" => $keyword], true);
    }
    public static function getPostImg($post){
        if( !empty( $post->news_image) )
            return $post->news_image;
        $id = $post->news_id;
        $path = "/news/$id/$id.jpg";
        return Helper::img($path);
    }
	public static function img($path)
	{
		$path_dir = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/img".$path;
		if(file_exists($path_dir))
			return "/files/img".$path;
		else
			return "/files/img/no_images.jpg";
	}

	public static function video($path)
	{
		$path_dir = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/videos".$path;
		if(file_exists($path_dir))
			return "/files/videos".$path;
		else
			return "/files/videos/no_video.mp4";
	}



	public static function getIp()
	{
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    } else {
	        $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    $ipr = explode(",", $ip);
	    if(count($ipr) > 0)
	        $ip = $ipr[0];
	    return $ip;
	}
    public static function cateToArray($obj){
        $cate = [];
        foreach($obj as $ca){
            $cate[] = $ca->cate_id;
        }
        return $cate;
    }
	public static function jsonx($state, $msg, $dt)
	{
		$xra = array(
			"is_success" => $state,
			"msg" => $msg, 
			"data" => $dt
		);

		header('Content-Type: application/json');
		echo json_encode($dt);
		die;
	}
	
	public static function base64_to_jpeg($base64_string, $output_file) {
	    $ifp = fopen($output_file, "wb"); 

	    $data = explode(',', $base64_string);

	    fwrite($ifp, base64_decode($data[1])); 
	    fclose($ifp); 

	    return $output_file; 
	}

	public static function gen_thumb($path, $path_target){
        // *** Open JPG image
        $magicianObj = new \imageLib($path);

        // *** Resize to best fit then crop
        $magicianObj -> resizeImage(180, 240, 'crop-m');

        // *** Save resized image as a PNG
        $magicianObj -> saveImage($path_target);
	}

	public static function delete_file($path){
		if(file_exists($path))
			unlink($path);
	}
}