<?php
namespace common\helpers;

class AES {
	const KEY_AES = '2b9ff1e6b89947dfa9e84f3cf763fce2';

	public static function encrypt($encrypt) {
		$key = self::KEY_AES;
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$encrypt = str_pad ( $encrypt, (16 * (floor ( strlen ( $encrypt ) / 16 ) + (strlen ( $encrypt ) % 16 == 0 ? 2 : 1))), chr ( 16 - (strlen ( $encrypt ) % 16) ) );
		$encrypted = base64_encode ( mcrypt_encrypt ( MCRYPT_RIJNDAEL_128, pack ( "H*", $key ), $encrypt, MCRYPT_MODE_ECB, $iv ) );
		return $encrypted;
	}

	public static function decrypt($decrypt) {
		$key = self::KEY_AES;
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_128, pack ( "H*", $key ), base64_decode ( $decrypt ), MCRYPT_MODE_ECB, $iv );
		return $decrypted;
	}

	public static function export($data) {
		$data = AES::decrypt($data);
		$strs = explode('&', $data);
		$vars = [];
		foreach ($strs as $str) {
			$tmp = explode('=', $str);
			if (isset($tmp[0]) && isset($tmp[1])) {
				$vars[$tmp[0]] = $tmp[1];
			}
		}

		return $vars;
	}
}