<?php

namespace backend\controllers;

use Yii;
use common\models\Advertise;
use common\models\AdvertiseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;

/**
 * AdvertiseController implements the CRUD actions for Advertise model.
 */
class AdvertiseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['update', 'index', 'create', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::role_superadmin)
                                return true;
                            return false;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Advertise models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvertiseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advertise model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advertise model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Advertise();
        if(!empty($_POST))
            $model->expire = $_POST['expire'] ? strtotime($_POST['expire']) : 0;

        if ($model->load($_POST) && $model->save()) {
           
            if(!empty($_FILES['img']["tmp_name"])){
                $path = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/img/advertise/";
                if(!file_exists($path))
                    mkdir($path, 0777);
                $file = $path."/".$model->id.".jpg";
                $model->image = $model->id.".jpg";
                $model->save();
                //$this->image = $file;
                move_uploaded_file($_FILES["img"]["tmp_name"], $file);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Advertise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(!empty($_POST))
            $model->expire = $_POST['expire'] ? strtotime($_POST['expire']) : 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($_FILES['img']["tmp_name"])){
                $path = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/img/advertise/";
                if(!file_exists($path))
                    mkdir($path, 0777);
                $random = uniqid();
                $file = $path."/".$random.".jpg";
                $model->image = "/files/img/advertise/".$random.".jpg";
                //var_dump($model->image);die;
                $model->save();
                move_uploaded_file($_FILES["img"]["tmp_name"], $file);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Advertise model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Advertise model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advertise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advertise::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    // public function getImageurl()
    // {
    //     return '/files/img/advertise/'.'10.img';
    // }
}
