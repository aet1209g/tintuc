<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goMainPage();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->goMainPage();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function goMainPage(){
        if(Yii::$app->user->identity->role_id == User::role_superadmin)
            return $this->redirect(['news/index']);

        if(Yii::$app->user->identity->role_id == User::role_acceptor)
            return $this->redirect(['news/index']);

        if(Yii::$app->user->identity->role_id == User::role_publisher)
            return $this->redirect(['news/index']);

        if(Yii::$app->user->identity->role_id == User::role_editor)
            return $this->redirect(['news/index']);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();

        if ($model->load(Yii::$app->request->post())) {
         //$model->save();
          $model->register();
           //print_r($model);die;
           return $this->redirect(['login']);
        }
        return $this->render('register', [
            'model' => $model,
        ]);

    }
}
