<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Tags;
use common\models\TagNews;
use common\models\News;
use common\models\NewsSearch;
use common\models\CateNews;
use common\models\HotNews;
use common\helpers\Helper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;
use arturoliveira\ExcelView;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['update', 'index', 'view', 'create', 'changestatus', 'tagslist', 'newslist', 'hot', 'export'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            //if (Yii::$app->user->identity->role_id == User::role_superadmin || Yii::$app->user->identity->role_id == User::role_editor)
                                return true;
                            //return false;
                        }
                    ],
                    [
                        'actions' => ['index', 'changestatus', 'view', 'tagslist'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::role_acceptor || Yii::$app->user->identity->role_id == User::role_publisher || Yii::$app->user->identity->role_id == User::role_superadmin)
                                return true;
                            return false;
                        }
                    ],
                    [
                        'actions' => ['paid'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::role_publisher || Yii::$app->user->identity->role_id == User::role_superadmin)
                                return true;
                            return false;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionPaid()
    {   
        if(!empty(Yii::$app->request->queryParams['paid'])){
            foreach (Yii::$app->request->queryParams['paid'] as $k => $v) {
                if(is_numeric($v)){
                    $md = News::find()->where(['news_id' => $k])->one();
                    $md->money = $v;
                    $md->paid_by = Yii::$app->user->identity->username;
                    $md->paid_at = time();
                    $md->save();
                }
            }
        }

        $_GET['NewsSearch']['news_status'] = News::status_published;
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = News::find()->select("SUM(news_viewed) as total_viewed, SUM(money) as total_money, COUNT(news_id) as total")->where([
                "news_status" => News::status_published
            ])
            ->andFilterWhere(["LIKE", "news_title", !empty(Yii::$app->request->queryParams['NewsSearch']['news_title']) ? Yii::$app->request->queryParams['NewsSearch']['news_title'] : ''])
            ->andFilterWhere(["LIKE", "created_by", !empty(Yii::$app->request->queryParams['NewsSearch']['created_by']) ? Yii::$app->request->queryParams['NewsSearch']['created_by'] : ''])
            ->asArray()->one();

        if(!empty($_GET['export_excel']))
            ExcelView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'fullExportType'=> 'xlsx', //can change to html,xls,csv and so on
                'grid_mode' => 'export',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'news_title',
                    'created_by',
                    'news_viewed',
                    'money'
                  ],
            ]);

        return $this->render('paid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model->news_id && $model->created_by != Yii::$app->user->identity->username && Yii::$app->user->identity->role_id == User::role_editor)
            return $this->redirect(['index']);
        
        $tmp = TagNews::find()->select("tag_id")->where(['news_id' => $id])->all();
        $tmp_ids = [];
        foreach ($tmp as $k => $v) {
            $tmp_ids[] = $v->tag_id;
        }

        if(count($tmp_ids) > 0)
            $tmp = Tags::find()->select("tag_title as text, tag_id as id")->where('tag_id IN ('. implode(',', $tmp_ids).') AND tag_status = '. Tags::status_active)->limit(20)->asArray()->all();

        $tmp_arr = [
            "id" => [],
            "text" => []
        ];

        foreach ($tmp as $k => $v) {
            $tmp_arr['text'][] = $v['text'];
            $tmp_arr['id'][] = $v['id'];
        }


        if(($model->news_status == News::status_accept && Yii::$app->user->identity->role_id == User::role_editor) || Yii::$app->user->identity->role_id == User::role_acceptor)
            return $this->render('view', [
                'model' => $model,
                'tags' => $tmp_arr
            ]);

        return $this->render('update', [
            'model' => $model,
            'tags' => $tmp_arr
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if(!empty($_POST['News'])){
            if(!empty($_POST['News']['other_news'])){
                $model->other_news = implode(',', array_filter($_POST['News']['other_news']));
                unset($_POST['News']['other_news']);
            }

            if(!empty($_POST['News']['news_status'])){
                $status = $_POST['News']['news_status'];
                
                if($status == News::status_published){
                    $model->published_by = Yii::$app->user->identity->username;
                    $model->published_at = time();
                }

                if($status == News::status_accept){
                    $model->accepted_by = Yii::$app->user->identity->username;
                    $model->accepted_at = time();
                }
            }
        }

        if ($model->load($_POST) && $model->save()) {

            if(!empty($_FILES['img']["tmp_name"]) && !$model->news_image){
                $path = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/img/news/".$model->news_id;
                if(!file_exists($path))
                    mkdir($path, 0777);
                $file = $path."/".$model->news_id.".jpg";
                $file_thumb = $path."/".$model->news_id."_thumb.jpg";
                $finfo = finfo_open(FILEINFO_MIME_TYPE); 
                $mime = finfo_file($finfo, $_FILES['img']['tmp_name']);
                finfo_close($finfo);
                if(move_uploaded_file($_FILES["img"]["tmp_name"], $file)){
                    if($mime == 'image/gif')
                        copy($file, $file_thumb);
                    else
                        Helper::gen_thumb($file, $file_thumb);
                }
            }


            if(!empty($_POST['categories']))
                foreach ($_POST['categories'] as $k => $v) {
                    $md = new CateNews;
                    $md->news_id = $model->news_id;
                    $md->cate_id = $v;
                    $md->save();
                }

            TagNews::deleteAll(['news_id' => $model->news_id]);
            if(!empty($_POST['tags']))
                foreach ($_POST['tags'] as $k => $v) {
                    if(!is_numeric($v)){
                        $tag = new Tags;
                        $tag->tag_title = $v;
                        $tag->save();
                        $v = $tag->tag_id;
                    }
                    $md = new TagNews;
                    $md->news_id = $model->news_id;
                    $md->tag_id = $v;
                    $md->save();
                }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tags' => ['id' => [], 'text' => []]
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->enableCsrfValidation = false;
        $model = $this->findModel($id);

        if(Yii::$app->user->identity->role_id == User::role_editor && $model->created_by != Yii::$app->user->identity->username)
            return $this->redirect(['index']);

        if(Yii::$app->user->identity->role_id == User::role_acceptor){
            $created_by_user = User::find()->where("username = '". $model->created_by."' AND role_id IN (".implode(',', [User::role_superadmin, User::role_publisher]).")")->one();
            if(!empty($created_by_user) || ($model->news_status == News::status_default && $model->created_by != Yii::$app->user->identity->username))
                return $this->redirect(['index']);
        }

        if(!empty($_POST['News'])){
            if(!empty($_POST['News']['other_news'])){
                $model->other_news = implode(',', array_filter($_POST['News']['other_news']));
                unset($_POST['News']['other_news']);
            }

            if(!empty($_POST['News']['news_status'])){
                $status = $_POST['News']['news_status'];
                
                if($status == News::status_published){
                    $model->published_by = Yii::$app->user->identity->username;
                    $model->published_at = time();
                }

                if($status == News::status_accept){
                    $model->accepted_by = Yii::$app->user->identity->username;
                    $model->accepted_at = time();
                }
            }
        }

        if ($model->load($_POST) && $model->save()) {
            
            if(!empty($_FILES['img']["tmp_name"]) && !$model->news_image){
                $path = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/img/news/".$model->news_id;
                if(!file_exists($path))
                    mkdir($path, 0777);
                $file = $path."/".$model->news_id.".jpg";
                $file_thumb = $path."/".$model->news_id."_thumb.jpg";
                $finfo = finfo_open(FILEINFO_MIME_TYPE); 
                $mime = finfo_file($finfo, $_FILES['img']['tmp_name']);
                finfo_close($finfo);
                if(move_uploaded_file($_FILES["img"]["tmp_name"], $file)){
                    if($mime == 'image/gif')
                        copy($file, $file_thumb);
                    else
                        Helper::gen_thumb($file, $file_thumb);
                }
            }

            if($model->news_image){
                $path = $_SERVER['DOCUMENT_ROOT']."/backend/web/files/img/news/".$model->news_id;
                $file = $path."/".$model->news_id.".jpg";
                $file_thumb = $path."/".$model->news_id."_thumb.jpg";
                Helper::delete_file($file);
                Helper::delete_file($file_thumb);
            }
            
            CateNews::deleteAll('news_id = '.$model->news_id);
            if(!empty($_POST['categories']))
                foreach ($_POST['categories'] as $k => $v) {
                    $md = new CateNews;
                    $md->news_id = $model->news_id;
                    $md->cate_id = $v;
                    $md->save();
                }

            TagNews::deleteAll(['news_id' => $model->news_id]);
            if(!empty($_POST['tags']))
                foreach ($_POST['tags'] as $k => $v) {
                    if(!is_numeric($v)){
                        $tag = new Tags;
                        $tag->tag_title = $v;
                        $tag->save();
                        $v = $tag->tag_id;
                    }
                    $md = new TagNews;
                    $md->news_id = $model->news_id;
                    $md->tag_id = $v;
                    $md->save();
                }
            return $this->redirect(['index']);
        } else {
            $tmp = TagNews::find()->select("tag_id")->where(['news_id' => $id])->all();
            $tmp_ids = [];
            foreach ($tmp as $k => $v) {
                $tmp_ids[] = $v->tag_id;
            }

            if(count($tmp_ids) > 0)
                $tmp = Tags::find()->select("tag_title as text, tag_id as id")->where('tag_id IN ('. implode(',', $tmp_ids).') AND tag_status = '. Tags::status_active)->limit(20)->asArray()->all();

            $tmp_arr = [
                "id" => [],
                "text" => []
            ];

            foreach ($tmp as $k => $v) {
                $tmp_arr['text'][] = $v['text'];
                $tmp_arr['id'][] = $v['id'];
            }


            return $this->render('update', [
                'model' => $model,
                'tags' => $tmp_arr
            ]);
        }
    }


    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionHot()
    {
        $model = HotNews::find()->one();
        if(Yii::$app->user->identity->role_id == User::role_editor)
            return $this->redirect(['index']);


        if (Yii::$app->request->post()) {
            if(!empty($_POST['news']))
                $model->special_news = implode(',', array_filter($_POST['news']));

            $model->hotest = Yii::$app->request->post()['HotNews']['hotest'];
            $model->others = implode(',', Yii::$app->request->post()['HotNews']['others']);
            $model->save();
            return $this->redirect(['hot']);
        }

        return $this->render('hot', [
            'model' => $model
        ]);
        
    }


    function actionChangestatus($status, $news_id){

        if($status < 0 || $news_id < 0)
            die("#401 - Bad request!");
        else{
            $model = $this->findModel($news_id);
            $model->news_status = $status;
            
            if($status == News::status_published){
                $model->published_by = Yii::$app->user->identity->username;
                $model->published_at = time();
            }

            if($status == News::status_accept){
                $model->accepted_by = Yii::$app->user->identity->username;
                $model->accepted_at = time();
            }

            $model->save();
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        FileHelper::removeDirectory($_SERVER['DOCUMENT_ROOT']."/backend/web/img/news/".$id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTagslist($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($q)) {
            $data = Tags::find()->select("tag_title as text, tag_id as id")->where("tag_title LIKE '%".trim($q)."%' AND tag_status = " . Tags::status_active)->limit(20)->asArray()->all();
            $out['results'] = array_values($data);
        }
        else if ($id) {
            $data = Tags::find()->select("tag_title as text")->where('tag_id IN ('. $id.') AND tag_status = '. Tags::status_active)->limit(20)->asArray()->all();
            $out['results'] = array_values($data);
        }
        return $out;
    }


    public function actionNewslist($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => []];
        if (!is_null($q)) {
            $data = News::find()->where("news_title LIKE '%".trim($q)."%' AND news_status = ". News::status_published)->limit(20)->asArray()->all();
        }
        else if ($id) {
            $data = News::find()->where('news_id IN ('. $id.') AND news_status = '. News::status_published)->limit(20)->asArray()->all();
        }

        if(is_null($q) && !$id)
            $data = News::find()->where('news_status = '. News::status_published)->orderBy(['updated_at' => SORT_DESC])->limit(20)->asArray()->all();

        foreach ($data as $k => $v) {
            $out['results'][] = [
                'id' => $v['news_id'],
                'text' => $v['news_title'],
                'description' => $v['news_description'],
                'link' => "http://".$_SERVER['HTTP_HOST'].'/bai-viet/'.$v['news_slug'].'.html',
                'thumb' => $v['news_image'] ? $v['news_image'] : "http://".$_SERVER['HTTP_HOST'].Helper::img('/news/'.$v['news_id'].'/'.$v['news_id'].'_thumb.jpg')
            ];
        }

        return $out;
    }
}
