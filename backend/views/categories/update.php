<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = 'Cập nhật chuyên mục: ' . $model->cate_title;
$this->params['breadcrumbs'][] = ['label' => 'Chuyên mục', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->cate_title;
?>
<div class="box">
	<div class="box-body categories-update">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
