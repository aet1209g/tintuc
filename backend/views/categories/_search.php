<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CategoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cate_id') ?>

    <?= $form->field($model, 'cate_title') ?>

    <?= $form->field($model, 'cate_slug') ?>

    <?= $form->field($model, 'cate_keywords') ?>

    <?= $form->field($model, 'cate_description') ?>

    <?php // echo $form->field($model, 'cate_robots') ?>

    <?php // echo $form->field($model, 'cate_total_news') ?>

    <?php // echo $form->field($model, 'add_user_id') ?>

    <?php // echo $form->field($model, 'add_username') ?>

    <?php // echo $form->field($model, 'add_datetime') ?>

    <?php // echo $form->field($model, 'cate_slug_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
