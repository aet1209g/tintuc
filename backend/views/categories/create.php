<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = 'Tạo chuyên mục';
$this->params['breadcrumbs'][] = ['label' => 'Chuyên mục', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
	<div class="box-body categories-create">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
