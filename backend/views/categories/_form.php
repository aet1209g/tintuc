<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cate_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cate_slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cate_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cate_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cate_status')->dropdownList(Categories::getListStatus()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tạo' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
