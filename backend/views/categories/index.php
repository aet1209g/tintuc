<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chuyên mục';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="categories-index">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div class="box-header with-border">
            <?= Html::a('Tạo chuyên mục', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'cate_id',
                    'cate_title',
                    [
                        'attribute' => "cate_status",
                        'value' => function($md){
                            return Categories::getStatusByID($md->cate_status);
                        },
                        'filter' => Categories::getListStatus(),
                    ],
                    // 'cate_robots',
                    // 'cate_total_news',
                    // 'add_user_id',
                    // 'add_username',
                    // 'add_datetime',
                    // 'cate_slug_id',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>