<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsMain */

$this->title = 'Update News Main: ' . $model->news_id;
$this->params['breadcrumbs'][] = ['label' => 'News Mains', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->news_id, 'url' => ['view', 'id' => $model->news_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-main-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
