<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use common\helpers\Helper;
use common\models\News;
use common\models\User;
use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nhuận bút';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
        <div class="news-index" >
            <div class="box-header with-border">
                <?= Html::a('Xuất file excel', Yii::$app->request->hostInfo . Yii::$app->request->url.
                    ($_SERVER['QUERY_STRING'] ? "&export_excel=1" : "?export_excel=1"), 
                    ['class' => 'btn btn-sm btn-primary']) 
                ?>
                <?= Html::submitButton('Lưu và tải lại trang', ['class' => 'btn btn-sm btn-success']); ?>
                <br><br>
                Tổng xem: <?=$data['total_viewed'];?>
                <br>
                Tổng tiền: <?=number_format($data['total_money']);?> VNĐ
            </div>
            <div class="box-body">
                <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'published_at',
                                'value' => function($model){
                                    return Date("d-m-Y H:i", $model->published_at);
                                },
                                'filter' => DateRangePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'published_at',
                                    'convertFormat' => true,
                                    'pluginOptions' => [
                                        'locale' => [
                                            'format' => 'd-m-Y'
                                        ],
                                    ],
                                ]),
                            ],
                            'news_title',
                            'created_by',
                            [
                                'attribute' => 'news_viewed',
                                'filter' => false
                            ],
                            [
                                'attribute' => 'money',
                                'format' => 'raw',
                                'value' => function($model){
                                    $str = "
                                    <input name='paid[".$model->news_id."]' type=number onblur='$(this).attr(\'disabled\', true)' onclick='$(this).attr(\'disabled\', false)' value='".$model->money."'>
                                    ";
                                    return $str;
                                },
                                'filter' => News::getListStatus(),
                            ]
                        ],
                    ]);
                ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>