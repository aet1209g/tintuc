<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\helpers\Helper;
use common\models\HotNews;
use common\models\News;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Tin nổi bật';
$this->params['breadcrumbs'][] = ['label' => 'Tin tức', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-body news-create">
        <div class="news-form">


            <?php $form = ActiveForm::begin([
                        'options' => ['enctype'=>'multipart/form-data']
            ]); ?>

            <br>
            <?php
                $url = \yii\helpers\Url::to(['newslist']);
                echo $form->field($model, 'hotest')->widget(Select2::classname(), [
                    'initValueText' => News::find()->where("news_id = ".$model->hotest)->one()->news_title, // set the initial display text
                    'options' => ['placeholder' => 'Tìm một bài viết hot nhất ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Đang tìm...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]);
         
            ?>
            <?php
                $url = \yii\helpers\Url::to(['newslist']);
                $str_id = $model->others;
                if($model->others)
                    $model->others = explode(",", $model->others);
                echo $form->field($model, 'others')->widget(Select2::classname(), [
                    'initValueText' => News::getListTitle($str_id), // set the initial display text
                    'options' => ['placeholder' => 'Chọn 4 bài viết khác ...', 'multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'maximumSelectionLength' => 4,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Đang tìm...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]);
         
            ?>
            <br><br>
            <h3>Tin nóng (hàng dọc)</h3>

            <?php
                $ids = array_filter(explode(',', $model->special_news));

                for($i = 0; $i <= 3; $i++){
                    echo '<br><label class="control-label">Bài viết '.($i+1).':</label>';
                    $value = null;
                    $text = "";
                    if(count($ids) >= $i + 1){
                        $tmp = News::find()->where("news_id = ".$ids[$i])->one();
                        $value = $tmp->news_id;
                        $text = $tmp->news_title;
                    }

                    echo Select2::widget([
                        'initValueText' =>  $text, // set the initial display text
                        'value' =>  $value, // set the initial display text
                        'name' => 'news[]',
                        'options' => ['placeholder' => 'Chọn một bài viết ...', 'multiple' => false],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Đang tìm...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]);
                }
            ?>
            <br>
            <br>
            <button class="btn btn-primary">Lưu</button>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<style type="text/css">
    .select2-container .select2-selection--single .select2-selection__rendered {
        margin-top: 0;
    }

    .select2-container--krajee .select2-selection__clear {
        line-height: 21px;
    }
</style>