<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\helpers\Helper;
use common\models\News;
use common\models\User;


/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tin tức';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="news-index">
        <?php if(Yii::$app->user->identity->role_id == User::role_superadmin):?>
            <div class="box-header with-border">
                <?= Html::a('Tạo tin tức', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
                <?= Html::a('Tin nổi bật', ['hot'], ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <div class="box-body">
                <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'news_title',
                            [
                                'attribute' => 'news_status',
                                'format' => 'raw',
                                'value' => function($md){
                                    return News::getStatusByID($md->news_status);
                                },
                                'filter' => News::getListStatus(),
                            ],
                            'created_by',
                            'news_viewed',
                            [
                                'label' => '',
                                'format' => 'raw',
                                'value' => function($model){
                                    $str = "";
                                    $str_dangbai = '<a class="label label-primary" href="'.Url::to(["news/changestatus", "status" => News::status_published, "news_id" => $model->news_id]).'">Đăng</a>';
                                    $str_gobai = '<a class="label label-danger" href="'.Url::to(["news/changestatus", "status" => News::status_rejected, "news_id" => $model->news_id]).'">Gỡ bài</a>';
                                    if($model->news_status == News::status_published)
                                        $str =  $str_gobai;
                                    else
                                        $str =  $str_dangbai;
                                    
                                    return $str;
                                },
                                'filter' => News::getListStatus(),
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                            ],
                        ],
                    ]);
                ?>
            </div>
        <?php endif;?>

        <?php if(Yii::$app->user->identity->role_id == User::role_publisher):?>
            <div class="box-header with-border">
                <?= Html::a('Tạo tin tức', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
                <?= Html::a('Tin nổi bật', ['hot'], ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <div class="box-body">
                <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'news_id',
                            'news_title',
                            'created_by',
                            'news_viewed',
                            [
                                'attribute' => 'news_status',
                                'format' => 'raw',
                                'value' => function($md){
                                    return News::getStatusByID($md->news_status);
                                },
                                'filter' => News::getListStatus(),
                            ],
                            [
                                'label' => '',
                                'format' => 'raw',
                                'value' => function($model){
                                    $str = "";
                                    $str_dangbai = '<a class="label label-primary" href="'.Url::to(["news/changestatus", "status" => News::status_published, "news_id" => $model->news_id]).'">Đăng</a>';
                                    $str_gobai = '<a class="label label-danger" href="'.Url::to(["news/changestatus", "status" => News::status_rejected, "news_id" => $model->news_id]).'">Gỡ bài</a>';
                                    if($model->news_status == News::status_published)
                                        $str =  $str_gobai;
                                    else
                                        $str =  $str_dangbai;
                                    
                                    return $str;
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                            ],
                        ],
                    ]);
                ?>
            </div>
        <?php endif;?>

        <?php if(Yii::$app->user->identity->role_id == User::role_editor):?>
            <div class="box-header with-border">
                <?= Html::a('Tạo tin tức', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <div class="box-body">
                <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'news_id',
                            'news_title',
                            'news_viewed',
                            [
                                'attribute' => 'news_status',
                                'format' => 'raw',
                                'value' => function($md){
                                    return News::getStatusByID($md->news_status);
                                },
                                'filter' => News::getListStatus(),
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                            ],
                        ],
                    ]);
                ?>
            </div>
        <?php endif;?>

        <?php if(Yii::$app->user->identity->role_id == User::role_acceptor):?>
            <div class="box-header with-border">
                <?= Html::a('Tạo tin tức', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
            </div>
            <div class="box-body">
                <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'news_id',
                            'news_title',
                            'created_by',
                            'news_viewed',
                            [
                                'attribute' => 'news_status',
                                'format' => 'raw',
                                'value' => function($md){
                                    return News::getStatusByID($md->news_status);
                                },
                                'filter' => News::getListStatus(),
                            ],
                            [
                                'label' => '',
                                'format' => 'raw',
                                'value' => function($model){
                                    $str_duyet = '<a class="label label-primary" href="'.Url::to(["news/changestatus", "status" => News::status_accept, "news_id" => $model->news_id]).'">Duyệt</a>';
                                    $str_huy = '<a class="label label-danger" href="'.Url::to(["news/changestatus", "status" => News::status_cancel, "news_id" => $model->news_id]).'">Không duyệt</a>';
                                    
                                    $str = "";
                                    if($model->news_status == News::status_accept)
                                        $str =  $str_huy;
                                    
                                    if($model->news_status == News::status_cancel || $model->news_status == News::status_pending)
                                        $str =  $str_duyet;
                                    
                                    return $str;
                                },
                                'filter' => News::getListStatus(),
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                            ],
                        ],
                    ]);
                ?>
            </div>
        <?php endif;?>
    </div>
</div>