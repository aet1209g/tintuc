<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Helper;
use yii\web\JsExpression;
use dosamigos\tinymce\TinyMce;
use common\models\Categories;
use common\models\News;
use common\models\User;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = 'Tin tức: ' . $model->news_title;
$this->params['breadcrumbs'][] = ['label' => 'Tin tức', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Cập nhật tin tức';
?>
<div class="box">
    <div class="box-body news-update">

        <div class="news-form">
            <?php $form = ActiveForm::begin([
                        'options' => ['enctype'=>'multipart/form-data']
            ]); ?>

            <div class="row">
                <div class="col-md-12">
                    <label>Ảnh đại diện</label>
                </div>
            </div>
            <img src="<?=Helper::img('/news/'.$model->news_id.'/'.$model->news_id.'.jpg').'?t='.time();?>" style="height: 250px" id="preview">
            <br><br>

            <?= $form->field($model, 'news_title')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'news_slug')->textInput(['maxlength' => true, 'readonly' => true]) ?>

            <?= $form->field($model, 'news_description')->textarea(['rows' => 6, 'readonly' => true]) ?>

            <?php 
                echo $form->field($model, 'news_content')->widget(Tinymce::className(), [
                    'options' => ['rows' => 15],
                    //'language' => 'es',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime image media table contextmenu paste filemanager"
                        ],
                        'image_advtab' => true, 
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link media image | fullscreen"
                    ]
                ]);
            ?>
            <br>
            <label class="control-label">Chuyên mục:</label>
            <?php
                echo Select2::widget([
                    'name' => 'categories',
                    'value' => Categories::getList($model->news_id),
                    'data' => Categories::getList(),
                    'size' => Select2::MEDIUM,
                    'disabled' => true,
                    'options' => ['placeholder' => 'Chọn một vài chuyên mục ...', 'multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ]);
            ?>
    <br>
    <label class="control-label">Chủ đề / Google keywords SEO:</label>
    <?php
        $url = Url::to(['tagslist']);
        $ids = implode(',', $tags['id']);
        $initScript =
<<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id={$ids}", {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
        echo Select2::widget([
            'name' => 'tags',
            "initValueText" => $tags['text'],
            "value" => $tags['id'],
            'size' => Select2::MEDIUM,
            'disabled' => true,
            'options' => ['placeholder' => 'Chọn một vài chủ đề ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                ],
                'initSelection' => new JsExpression($initScript),
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { console.log(city); return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]);
    ?>
    <br>

        <?php
            $url = \yii\helpers\Url::to(['newslist']);
            $str_id = $model->other_news;
            if($model->other_news)
                $model->other_news = explode(",", $model->other_news);
            echo $form->field($model, 'other_news')->widget(Select2::classname(), [
                'initValueText' => News::getListTitle($str_id), // set the initial display text
                'options' => ['placeholder' => 'Chọn 3 bài viết khác ...', 'multiple' => true],
                'disabled' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                    'maximumSelectionLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Đang tìm...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ]);
     
        ?>
    <br>
            <div class="form-group">
                <?php if((Yii::$app->user->identity->role_id == User::role_superadmin || Yii::$app->user->identity->role_id == User::role_acceptor) 
                        && ($model->news_status == News::status_pending || $model->news_status == News::status_accept || $model->news_status == News::status_cancel)): ?>
                    <?php if($model->news_status != News::status_accept):?><a href="<?=Url::to(["news/changestatus", "status" => News::status_accept, "news_id" => $model->news_id])?>" class="btn btn-primary">Duyệt</a><?php endif;?>
                    <?php if($model->news_status != News::status_cancel):?><a href="<?=Url::to(["news/changestatus", "status" => News::status_cancel, "news_id" => $model->news_id])?>" class="btn btn-danger">Không duyệt</a><?php endif;?>
                <?php endif; ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
