<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = 'Tạo tin tức';
$this->params['breadcrumbs'][] = ['label' => 'Tin tức', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
	<div class="box-body news-create">
	    <?= $this->render('_form', [
	        'model' => $model,
	        'tags' => $tags
	    ]) ?>

	</div>
</div>