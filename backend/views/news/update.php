<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = 'Cập nhật tin tức: ' . $model->news_title;
$this->params['breadcrumbs'][] = ['label' => 'Tin tức', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Cập nhật tin tức';
?>
<div class="box">
	<div class="box-body news-update">
	    <?= $this->render('_form', [
	        'model' => $model,
            'tags' => $tags
	    ]) ?>
	</div>
</div>
