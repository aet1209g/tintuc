<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'news_id') ?>

    <?= $form->field($model, 'news_title') ?>

    <?= $form->field($model, 'news_slug') ?>

    <?= $form->field($model, 'news_content') ?>

    <?= $form->field($model, 'new_image') ?>

    <?php // echo $form->field($model, 'news_keywords') ?>

    <?php // echo $form->field($model, 'news_description') ?>

    <?php // echo $form->field($model, 'news_robots') ?>

    <?php // echo $form->field($model, 'news_viewed') ?>

    <?php // echo $form->field($model, 'news_tagid') ?>

    <?php // echo $form->field($model, 'news_tagtitle') ?>

    <?php // echo $form->field($model, 'cate_id') ?>

    <?php // echo $form->field($model, 'cate_title') ?>

    <?php // echo $form->field($model, 'cate_slug') ?>

    <?php // echo $form->field($model, 'add_userid') ?>

    <?php // echo $form->field($model, 'add_username') ?>

    <?php // echo $form->field($model, 'add_datetime') ?>

    <?php // echo $form->field($model, 'add_date_int') ?>

    <?php // echo $form->field($model, 'public_datetime') ?>

    <?php // echo $form->field($model, 'public_datetime_int') ?>

    <?php // echo $form->field($model, 'news_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
