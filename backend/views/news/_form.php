<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\helpers\Helper;
use dosamigos\tinymce\TinyMce;
use common\models\Categories;
use common\models\News;
use common\models\User;
use common\models\Menu;
use kartik\select2\Select2;

if(!$model->news_description)
    $model->news_description = "(Doanhnghiep.net.vn) - ";
/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */

$status = [];

switch (Yii::$app->user->identity->role_id) {
    case User::role_publisher:
    case User::role_superadmin:
        if($model->isNewRecord)
            $status = [
                News::status_default => 'Chỉ tạo',
                News::status_published => "Tạo & đăng bài"
            ];
        else{
            if($model->news_status == News::status_published)
                $status = [
                    $model->news_status => 'Chỉ lưu',
                    News::status_rejected => "Lưu & gỡ bài"
                ];
            if($model->news_status != News::status_published)
                $status = [
                    $model->news_status => 'Chỉ lưu',
                    News::status_published => "Lưu & đăng bài"
                ];
        }
        break;
    case User::role_editor:
        if($model->isNewRecord)
            $status = [
                News::status_default => 'Chỉ tạo',
                News::status_pending => "Tạo & nộp bài"
            ];
        else{
            if($model->news_status != News::status_default)
                $status = [];
            if($model->news_status == News::status_default || $model->news_status == News::status_rejected || $model->news_status == News::status_cancel)
                $status = [
                    $model->news_status => 'Chỉ lưu',
                    News::status_pending => "Lưu & nộp bài"
                ];
        }
        break;
    case User::role_acceptor:
        if($model->isNewRecord)
            $status = [
                News::status_default => 'Chỉ tạo',
                News::status_accept => "Tạo & duyệt bài"
            ];
        else{
            if($model->news_status == News::status_accept)
                $status = [
                    $model->news_status => 'Chỉ lưu',
                    News::status_cancel => "Lưu & không duyệt"
                ];

            if($model->news_status == News::status_default || $model->news_status == News::status_pending || $model->news_status == News::status_rejected || $model->news_status == News::status_cancel)
                $status = [
                    $model->news_status => 'Chỉ lưu',
                    News::status_accept => "Lưu & duyệt bài"
                ];
        }
        break;
    
}
?>

<div class="news-form">


    <?php $form = ActiveForm::begin([
                'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <label onclick="takePick()">Ảnh đại diện (640x480 | 240x180)</label>
            <br>
            <div class="clearfix">
                <input type="file" name="img" class="pull-left btn btn-sm btn-default" accept="image/*" id="files" onchange="dimgx(this)">
            </div>
        </div>
    </div>
    <br>
    <?php if(!$model->news_image): ?>
        <div class="preview" style="background: url(<?=Helper::img('/news/'.$model->news_id.'/'.$model->news_id.'_thumb.jpg').'?t='.time();?>)" id="preview_thumb"></div>
    <?php else: ?>
        <div class="preview" style="background: url(<?=$model->news_image.'?t='.time();?>)" id="preview_thumb"></div>
    <?php endif; ?>
    <br><br>

    <?= $form->field($model, 'news_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_slug')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_description')->textarea(['rows' => 6]) ?>

    <?php 
        echo $form->field($model, 'news_content')->widget(Tinymce::className(), [
            'options' => ['rows' => 25],
            'language' => 'vi',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media image imagetools table contextmenu paste filemanager preview template"
                ],
                'paste_retain_style_properties' => "color font-size",
                'image_advtab' => true, 
                'image_caption' => true,
                'paste_data_images' => true,
                'paste_auto_cleanup_on_paste' => true,
                'imagetools_toolbar' => "setthumb imageoptions",
                'extended_valid_elements'=> 'a[class|title|style|href]',
                'urlconverter_callback' => 'remove_link',
                'templates' => [
                    [
                        'title' => 'Box căn giữa',
                        'description' => '',
                        'content' => '<p></p><blockquote class="blockquote">Nhập nội dung box căn giữa</blockquote><p></p>'
                    ],
                    [
                        'title' => 'Box căn phải',
                        'description' => '',
                        'content' => '<p></p><blockquote class="blockquote box-right">Nhập nội dung box căn phải</blockquote><p></p>'
                    ]
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent template | link media image | fullscreen"
            ]
        ]);
    ?>
    <br>
    <br>
    <label class="control-label">Danh mục chính:</label>
    <?php
        echo Select2::widget([
            'name' => 'News[main_cate_id]',
            'value' => $model->main_cate_id,
            'data' => Categories::getList(),
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Chọn một chuyên mục ...', 'multiple' => false],
            'pluginOptions' => [
                'maximumInputLength' => 1000,
                'allowClear' => true
            ],
        ]);
    ?>
    <br>
    <label class="control-label">Hiển thị trong các danh mục khác:</label>
    <?php
        echo Select2::widget([
            'name' => 'categories',
            'value' => Categories::getList($model->news_id ? $model->news_id : -1),
            'data' => Categories::getList(),
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Chọn một vài chuyên mục ...', 'multiple' => true],
            'pluginOptions' => [
                'maximumInputLength' => 1000,
                'allowClear' => true
            ],
        ]);
    ?>
    <br>
    <label class="control-label">Chủ đề / Google keywords SEO:</label>
    <?php
        $url = Url::to(['tagslist']);
        $ids = implode(',', $tags['id']);
        $initScript =
<<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id={$ids}", {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
        echo Select2::widget([
            'name' => 'tags',
            "initValueText" => $tags['text'],
            "value" => $tags['id'],
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Chọn một vài chủ đề ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'maximumInputLength' => 10000000,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                    'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                ],
                'initSelection' => new JsExpression($initScript),
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { console.log(city); return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]);
    ?>
    <br>

        <?php
            $url = \yii\helpers\Url::to(['newslist']);
            $str_id = $model->other_news;
            if($model->other_news)
                $model->other_news = explode(",", $model->other_news);
            echo $form->field($model, 'other_news')->widget(Select2::classname(), [
                'initValueText' => News::getListTitle($str_id), // set the initial display text
                'options' => ['placeholder' => 'Chọn 3 bài viết khác ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,
                    'maximumSelectionLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Đang tìm...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ]);
     
        if(!empty($status))
            echo $form->field($model, 'news_status')->dropDownList($status)->label('Hành động');
    ?>
    <br>
    <div style="display: none">
        <?= $form->field($model, 'news_image')->hiddenInput(['id' => 'news_image'])->label('') ?>
        <?= $form->field($model, 'updated_at')->hiddenInput(['value' => time()])->label('') ?>
        <?= $form->field($model, 'updated_by')->hiddenInput(['value' => Yii::$app->user->identity->username])->label('') ?>
        <?= $model->isNewRecord ? $form->field($model, 'created_at')->hiddenInput(['value' => time()])->label('') : "" ?>
        <?= $model->isNewRecord ? $form->field($model, 'created_by')->hiddenInput(['value' => Yii::$app->user->identity->username])->label('') : "" ?>
    </div>
    <div class="form-group">
        <?php 
            if(!empty($status))
                echo Html::submitButton($model->isNewRecord ? 'Tạo' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']);

            /*$str_tao_sua_baiviet = Html::submitButton($model->isNewRecord ? 'Tạo' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']);
            $str_dangbai = '<a href='.Url::to(["news/changestatus", "status" => News::status_published, "news_id" => $model->news_id]).' class="btn btn-primary">Đăng bài</a>';
            $str_gobai = '<a href='.Url::to(["news/changestatus", "status" => News::status_rejected, "news_id" => $model->news_id]).' class="btn btn-danger">Gỡ bài</a>';
            $str_duyet = '<a href='.Url::to(["news/changestatus", "status" => News::status_accept, "news_id" => $model->news_id]).' class="btn btn-primary">Duyệt</a>';
            $str_khongduyet = '<a href='.Url::to(["news/changestatus", "status" => News::status_cancel, "news_id" => $model->news_id]).' class="btn btn-danger">Không duyệt</a>';
            $str_nop = '<a href='.Url::to(["news/changestatus", "status" => News::status_pending, "news_id" => $model->news_id]).' class="btn btn-primary">Nộp bài</a>';
            echo $str_tao_sua_baiviet;
            switch (Yii::$app->user->identity->role_id) {
                case User::role_superadmin:
                    if($model->news_status == News::status_published)
                        echo $str_gobai;
                    else
                        echo $str_dangbai;
                    break;
                case User::role_editor:
                    if(!$model->isNewRecord && (News::status_default == $model->news_status || News::status_cancel == $model->news_status)){
                        echo $str_nop;
                    }
                    break;
                case User::role_publisher:
                    if(!$model->isNewRecord && ($model->news_status == News::status_accept || $model->news_status == News::status_rejected)){
                        echo $str_dangbai;
                    }

                    if(!$model->isNewRecord && $model->news_status == News::status_published)
                        echo $str_gobai;
                    break;
                
            }*/
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
        var u_img = "";
        function dimgx(e)
        {
            if (typeof (FileReader) != "undefined") {
                var img = $(".preview");

                if(!u_img){
                    u_img = $(".preview").css("background-image");
                    u_img = u_img.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
                }

                $("#news_image").val('');
                img.css("background",  "url("+u_img+")");

                $($(e)[0].files).each(function () {
                    var file = $(this);

                    if(file[0].size >= (20500 * 1024))
                    {
                        alert("Kích thước tệp tin (" + Math.round(file[0].size / 1024) + "KB) không được lớn hơn 500KB");
                        $("input[type=file]").val("");
                        return false;
                    }

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        img.css("background",  "url("+e.target.result+")");
                    }

                    reader.readAsDataURL(file[0]);
                });
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        }

        function setThumb(dt){
            return function(){
                var img = dt();
                var src = $(img).attr('src');
                $(".preview").css('background', 'url('+src+'?t='+ new Date().getTime()+')');
                $("#news_image").val(src);
                $("input[type=file]").val("");
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        }

        function remove_link(url, node, on_save, name){
             //Return new URL
            if( node == 'a' && url.split('://').length > 1 && url.split('doanhnghiep.net.vn').length == 1)
                url = '#';
            return url;
        }
</script>

<style type="text/css">
    #preview_thumb,
    #preview {
        height: 480px; width: 640px;
        background-size: cover !important;
        background-position: center center !important;
        display: inline-block;
        vertical-align: text-top;
    }

    #preview_thumb {
        height: 180px;
        width: 240px;
    }
</style>