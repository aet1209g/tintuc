<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Config */

$this->title = 'Thiết lập thông tin web';
$this->params['breadcrumbs'][] = 'Thiết lập thông tin web';
?>
<div class="box">
	<div class="box-body user-update">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
