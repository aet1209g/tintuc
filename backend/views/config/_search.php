<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ConfigSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'config_id') ?>

    <?= $form->field($model, 'config_key') ?>

    <?= $form->field($model, 'config_value') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'desciprtion') ?>

    <?php // echo $form->field($model, 'keyword') ?>

    <?php // echo $form->field($model, 'title_social') ?>

    <?php // echo $form->field($model, 'description_social') ?>

    <?php // echo $form->field($model, 'image_social') ?>

    <?php // echo $form->field($model, 'facebook_page') ?>

    <?php // echo $form->field($model, 'google_page') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
