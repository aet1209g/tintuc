<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Config */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'config_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'config_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'desciprtion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_social')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_social')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_social')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_page')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'google_page')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer_header')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer_content')->textArea(['rows' => '6']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tạo' : 'Lưu', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
