<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tài khoản';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="user-index">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div class="box-header with-border">
            <?= Html::a('Tạo tài khoản', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'username',
                    'fullname',
                    // 'password_reset_token',
                    'email:email',
                    [
                        'attribute' => "status",
                        'value' => function($md){
                            return User::getStatusByID($md->status);
                        },
                        'filter' => User::getListStatus(),
                    ],
                    [
                        'attribute' => "role_id",
                        'value' => function($md){
                            return User::getRoleByID($md->role_id);
                        },
                        'filter' => User::getListRole(),
                    ],
                    // 'created_at',
                    // 'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>