<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Cập nhật tài khoản: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Tài khoản', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->username;
?>
<div class="box">
	<div class="box-body user-update">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>