<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\grid\GridView;
use kartik\date\DatePicker;
use common\helpers\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\Advertise */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertise-form">

    <?php $form = ActiveForm::begin([
                'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    
    <div class="row">
        <div class="col-md-12">
            <label onclick="takePick()">Ảnh đại diện</label>
            <br>
            <div class="clearfix">
                <input type="file" name="img" class="pull-left btn btn-sm btn-default" accept="image/*" id="files" onchange="dimgx(this)">
            </div>
        </div>
    </div>
    <br>
    <div class="preview" style="background: url(<?=Helper::img('/advertise/'.$model->id.'.jpg').'?t='.time();?>)" id="preview_thumb">
        
    </div>
    <br><br>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'advertise_key')->textInput(['maxlength' => true]) ?>

    <label>Ngày hết hiệu lực</label>

    <?= DatePicker::widget([
        'name' => 'expire',
        'value' => $model->expire > 0 ? date('d-m-y', $model->expire) : '',
        'options' => ['placeholder' => 'Mời bạn chọn ngày ...'],
        'pluginOptions' => [
            'format' => 'dd-mm-yyyy',
            'todayHighlight' => true,
            'autoclose'=>true,
    ]
    ]);
    ?>
    <br><br>

    <!-- <label>Chọn ảnh quảng cáo mặc định</label>
    
    <div class="row">
        <div class="col-md-12">
            <label onclick="takePick()"></label>
            <input type="file" class="btn btn-sm btn-default" id="files" onchange="dimgx(this)">
            <input type="hidden" class="btn btn-sm btn-default" name="img" id="img">
            <input type="hidden" class="btn btn-sm btn-default" name="img_thumb" id="img_thumb">
        </div>
    </div>
    <br><br> -->
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<script type="text/javascript">
        var u_img = "";
        function dimgx(e)
        {
            if (typeof (FileReader) != "undefined") {
                var img = $(".preview");

                if(!u_img){
                    u_img = $(".preview").css("background-image");
                    u_img = u_img.replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
                }

                $("#news_image").val('');
                img.css("background",  "url("+u_img+")");

                $($(e)[0].files).each(function () {
                    var file = $(this);

                    if(file[0].size >= (20500 * 1024))
                    {
                        alert("Kích thước tệp tin (" + Math.round(file[0].size / 1024) + "KB) không được lớn hơn 500KB");
                        $("input[type=file]").val("");
                        return false;
                    }

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        img.css("background",  "url("+e.target.result+")");
                    }

                    reader.readAsDataURL(file[0]);
                });
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        }
</script>
</div>
<style type="text/css">
    #preview_thumb,
    #preview {
        height: 480px; width: 640px;
        background-size: cover !important;
        background-position: center center !important;
        display: inline-block;
        vertical-align: text-top;
    }

    #preview_thumb {
        height: 180px;
        width: 240px;
    }
</style>