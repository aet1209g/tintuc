<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Advertise */

$this->title = 'Quảng cáo';
// $this->params['breadcrumbs'][] = ['label' => 'Advertises', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Quảng cáo';
?>
<div class="advertise-update">
    <?= $this->render('view', [
        'model' => $model,
    ]) ?>

</div>

