<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Advertise */

$this->title = 'Tạo quảng cáo';
$this->params['breadcrumbs'][] = ['label' => 'Quảng cáo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
	<div class="box-body advertise-create">

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>	
