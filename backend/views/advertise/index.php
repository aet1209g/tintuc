<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AdvertiseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý quảng cáo';
$this->params['breadcrumbs'][] = 'Quảng cáo';
?>
<div class="box">
<div class="advertise-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    </p>
    <div class="box-body test">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'details',
                'link_advertise',

                array(
                    'format' => ['image',['width'=>'200']],
                    'value'=>function($model) { return $model->image; },
                    'attribute'=>'image',
                ),
                //'expire',
                [
                    'attribute'=>'expire',
                    'format'=>['DateTime','php:d-m-Y H:i:s']
                ],
                [
                    'attribute'=>'updated_at',
                    'format'=>['DateTime','php:d-m-Y H:i:s']
                ],
                
                ['class' => 'yii\grid\ActionColumn','template' => '{update}',],
            ],
        ]); ?>
    </div>
</div>
</div>
<style type="text/css">
    .test{
        overflow: auto;
        overflow-y: hidden;
    }
    .fix_class{
        width:100px;
    }
</style>