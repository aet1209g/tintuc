<?php

use common\models\User;
use common\models\News;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <!--
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>-->

        <!-- search form -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        -->
        <!-- /.search form -->

        <?php 

            if(Yii::$app->user->identity->role_id == User::role_superadmin){
                $count_accept = News::find()->where(['news_status' => News::status_accept])->count();
                $count_cancel = News::find()->where(['news_status' => News::status_cancel])->count();
                $count_pending = News::find()->where(['news_status' => News::status_pending])->count();
                $count_dadang = News::find()->where(['news_status' => News::status_published])->count();
                $count_chodang = News::find()->where(['news_status' => News::status_accept])->count();
                $count_huybo = News::find()->where(['news_status' => News::status_rejected])->count();

                echo dmstr\widgets\Menu::widget([
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Thống kê bài viết', 'options' => ['class' => 'header']],
                        ['encode' => false, 'label' => 'Đã duyệt/chờ đăng <small class="label pull-right label-primary">' . $count_accept . '</small>', 'icon' => 'fa fa-check-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_accept]],
                        ['encode' => false, 'label' => 'Chưa duyệt <small class="label pull-right label-warning">' . $count_pending . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_pending]],
                        ['encode' => false, 'label' => 'Không duyệt <small class="label pull-right label-danger">' . $count_cancel . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_cancel]],
                        ['encode' => false, 'label' => 'Đã đăng <small class="label pull-right label-primary">' . $count_dadang . '</small>', 'icon' => 'fa fa-check-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_published]],
                        ['encode' => false, 'label' => 'Bị gỡ <small class="label pull-right label-danger">' . $count_huybo . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_rejected]],
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Tài khoản', 'icon' => 'fa fa-user', 'url' => ['/user']],
                        ['label' => 'Menu điều hướng', 'icon' => 'fa fa-bars', 'url' => ['/menu']],
                        ['label' => 'Chuyên mục', 'icon' => 'fa fa-bookmark', 'url' => ['/categories']],
                        ['label' => 'Chủ đề\tags', 'icon' => 'fa fa-flag', 'url' => ['/tags']],
                        ['label' => 'Tin tức', 'icon' => 'fa fa-newspaper-o', 'url' => '#',
                            'items' => [
                                ['label' => 'Bài viết', 'icon' => 'fa', 'url' => ['/news']],
                                ['label' => 'Nhuận bút', 'icon' => 'fa', 'url' => ['/news/paid']],
                                ['label' => 'Tin nổi bật', 'icon' => 'fa', 'url' => ['/news/hot']],
                            ]
                        ],
                        ['label' => 'Cấu hình', 'icon' => 'fa fa-cogs', 'url' => ['/config']],
                        ['label' => 'Quảng cáo', 'icon' => 'fa fa-picture-o', 'url' => ['/advertise']],
                    ],
                ]);
            }

            if(Yii::$app->user->identity->role_id == User::role_acceptor){
                $base = News::find()->where("
                    (created_by NOT IN (SELECT username FROM user WHERE role_id IN (".implode(",", [User::role_publisher, User::role_superadmin]).")))
                    AND
                    news_status <> ".News::status_default."
                    OR
                    (news_status = ".News::status_default." AND created_by = '".Yii::$app->user->identity->username."')
                ");

                $count_accept = clone $base;
                $count_accept = $count_accept->andWhere(['news_status' => News::status_accept])->count();
                $count_cancel = clone $base;
                $count_cancel = $count_cancel->andWhere(['news_status' => News::status_cancel])->count();
                $count_pending = clone $base;
                $count_pending = $count_pending->andWhere(['news_status' => News::status_pending])->count();
                $count_huybo = clone $base;
                $count_huybo = $count_huybo->andWhere(['news_status' => News::status_rejected])->count();
                $count_mine = clone $base;
                $count_mine = $count_mine->andWhere(['created_by' => Yii::$app->user->identity->username])->count();

                echo dmstr\widgets\Menu::widget([
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Thống kê bài viết', 'options' => ['class' => 'header']],
                        ['encode' => false, 'label' => 'Đã duyệt <small class="label pull-right label-primary">' . $count_accept . '</small>', 'icon' => 'fa fa-check-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_accept]],
                        ['encode' => false, 'label' => 'Chưa duyệt <small class="label pull-right label-warning">' . $count_pending . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_pending]],
                        ['encode' => false, 'label' => 'Không duyệt <small class="label pull-right label-danger">' . $count_cancel . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_cancel]],
                        ['encode' => false, 'label' => 'Bị gỡ <small class="label pull-right label-danger">' . $count_huybo . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_rejected]],
                        ['encode' => false, 'label' => 'Bài viết của tôi <small class="label pull-right label-info">' . $count_mine . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[created_by]'=> Yii::$app->user->identity->username]],
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Tin tức', 'icon' => 'fa fa-newspaper-o', 'url' => ['/news']],
                    ],
                ]);
            }

            if(Yii::$app->user->identity->role_id == User::role_publisher){
                $base = News::find()->where("
                    news_status <> ".News::status_default."
                    OR
                    (news_status = ".News::status_default." AND created_by = '".Yii::$app->user->identity->username."')
                ");
                $count_dadang = clone $base;
                $count_dadang = $count_dadang->andWhere(['news_status' => News::status_published])->count();
                $count_chodang = clone $base;
                $count_chodang = $count_chodang->andWhere(['news_status' => News::status_accept])->count();
                $count_huybo = clone $base;
                $count_huybo = $count_huybo->andWhere(['news_status' => News::status_rejected])->count();
                $count_mine = clone $base;
                $count_mine = $count_mine->andWhere(['created_by' => Yii::$app->user->identity->username])->count();
                $count_cancel = clone $base;
                $count_cancel = $count_cancel->andWhere(['news_status' => News::status_cancel])->count();
                $count_pending = clone $base;
                $count_pending = $count_pending->andWhere(['news_status' => News::status_pending])->count();

                echo dmstr\widgets\Menu::widget([
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Thống kê bài viết', 'options' => ['class' => 'header']],
                        ['encode' => false, 'label' => 'Đã đăng <small class="label pull-right label-primary">' . $count_dadang . '</small>', 'icon' => 'fa fa-check-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_published]],
                        ['encode' => false, 'label' => 'Chưa đăng <small class="label pull-right label-warning">' . $count_chodang . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_accept]],
                        ['encode' => false, 'label' => 'Bị gỡ <small class="label pull-right label-danger">' . $count_huybo . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_rejected]],
                        ['encode' => false, 'label' => 'Bài viết của tôi <small class="label pull-right label-info">' . $count_mine . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[created_by]'=> Yii::$app->user->identity->username]],
                        ['encode' => false, 'label' => 'Chưa duyệt <small class="label pull-right label-warning">' . $count_pending . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_pending]],
                        ['encode' => false, 'label' => 'Không duyệt <small class="label pull-right label-danger">' . $count_cancel . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_cancel]],
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Tin tức', 'icon' => 'fa fa-newspaper-o', 'url' => ['/news']],
                        ['label' => 'Nhuận bút', 'icon' => 'fa fa-newspaper-o', 'url' => ['/news/paid']],
                    ],
                ]);
            }

            if(Yii::$app->user->identity->role_id == User::role_editor){
                $count_accept = News::find()->where(['news_status' => News::status_accept, "created_by" => Yii::$app->user->identity->username])->count();
                $count_cancel = News::find()->where(['news_status' => News::status_cancel, "created_by" => Yii::$app->user->identity->username])->count();
                $count_pending = News::find()->where(['news_status' => News::status_pending, "created_by" => Yii::$app->user->identity->username])->count();

                echo dmstr\widgets\Menu::widget([
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Thống kê bài viết', 'options' => ['class' => 'header']],
                        ['encode' => false, 'label' => 'Đã duyệt <small class="label pull-right label-primary">' . $count_accept . '</small>', 'icon' => 'fa fa-check-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_accept]],
                        ['encode' => false, 'label' => 'Chưa duyệt <small class="label pull-right label-warning">' . $count_pending . '</small>', 'icon' => 'fa fa-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_pending]],
                        ['encode' => false, 'label' => 'Không duyệt <small class="label pull-right label-danger">' . $count_cancel . '</small>', 'icon' => 'fa fa-times-circle-o', 'url' => ['news/index', 'NewsSearch[news_status]'=> News::status_cancel]],
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Tin tức', 'icon' => 'fa fa-newspaper-o', 'url' => ['/news']],
                    ],
                ]);
            }
        ?>

    </section>

</aside>
