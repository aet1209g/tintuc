<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Tags;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chủ đề';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="news-index">
        <div class="box-header with-border">
            <?= Html::a('Tạo chủ đề', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'tag_id',
                    'tag_title',
                    'tag_slug',
                    [
                        'attribute' => 'tag_status',
                        'format' => 'raw',
                        'value' => function($md){
                            return Tags::getStatusByID($md->tag_status);
                        },
                        'filter' => Tags::getListStatus(),
                    ],
                    'created_by',
                    // 'created_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
