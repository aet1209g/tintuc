<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tags */

$this->title = 'Cập nhật chủ đề: ' . $model->tag_title;
$this->params['breadcrumbs'][] = ['label' => 'Chủ đề', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Cập nhật chủ đề';
?>
<div class="box">
	<div class="box-body tags-update">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
