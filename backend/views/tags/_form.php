<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Tags;

/* @var $this yii\web\View */
/* @var $model common\models\Tags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tags-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tag_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tag_status')->dropdownList(Tags::getListStatus()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tạo' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
