<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\helpers\Helper;
use dosamigos\tinymce\TinyMce;
use common\models\Categories;
use kartik\select2\Select2;
use common\models\Menu;
use common\models\News;
$url = \yii\helpers\Url::to(['news/newslist']);

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'menu_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'menu_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'menu_css')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'menu_parent_id')->dropDownList(Menu::getList()) ?>

    <label class="control-label">Chuyên mục:</label>
    <?php
        echo Select2::widget([
            'name' => 'categories',
            'value' => Categories::getListWMenu($model->menu_id ? $model->menu_id : -1),
            'data' => Categories::getListWMenu(),
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Chọn một vài chuyên mục ...', 'multiple' => true],
            'pluginOptions' => [
                'maximumInputLength' => 10,
                'allowClear' => true
            ],
        ]);
    ?>
    <br><br>
    <h3>Bài viết nổi bật</h3>

    <?php
        $ids = array_filter(explode(',', $model->news_id));

        for($i = 0; $i <= 4; $i++){
            echo '<br><label class="control-label">Bài viết '.($i+1).':</label>';
            $value = null;
            $text = "";
            if(count($ids) >= $i + 1){
                $tmp = News::find()->where("news_id = ".$ids[$i])->one();
                $value = $tmp->news_id;
                $text = $tmp->news_title;
            }

            echo Select2::widget([
                'initValueText' =>  $text, // set the initial display text
                'value' =>  $value, // set the initial display text
                'name' => 'news[]',
                'options' => ['placeholder' => 'Chọn một bài viết ...', 'multiple' => false],
                'pluginOptions' => [
                    'allowClear' => true,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Đang tìm...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ]);
        }
    ?>
    <br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
