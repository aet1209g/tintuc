<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => '',
            'enableAutoLogin' => true,
            'loginUrl' => '/',  
            'identityCookie' => [
                'name' => '_frontendUser', // unique for backend
                'path'=>'/frontend/web'  // correct path for the backend app.
            ]
        ],
        'session' => [
            'name' => '_frontendSessionId', // unique for backend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'baseUrl' => $baseUrl,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'baseUrl' => $baseUrl,
            'showScriptName' => false,
            'rules' => [
            	'bai-viet/<slug>.html'=>'site/bai-viet',
            	'danh-muc/<slug>.html'=>'site/danh-muc',
                'tim-kiem.html'=>'site/tim-kiem',
            ],
        ],
    ],
    'params' => $params,
];

