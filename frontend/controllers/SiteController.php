<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\db\Query;
use yii\helpers\Url;
use yii\data\Pagination;

use common\models\Menu;
use common\models\News;
use common\models\Config;
use common\models\HotNews;
use common\models\Advertise;
use common\helpers\Helper;
/**
 * Site controller
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','bai-viet','danh-muc', 'tim-kiem', 'error'],
                        'allow' => true,
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        /*$allowed_hosts = array('demo.doanhnghiep.net.vn');
        if (!isset($_SERVER['HTTP_HOST']) || !in_array($_SERVER['HTTP_HOST'], $allowed_hosts)) {
            header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
            exit;
        }*/
        $this->view->params['menu'] = Menu::getMenu();
        if(isset($_GET['mobile']) &&  $_GET['mobile'] == true){
            $this->view->params['desktop'] =  1;
        }else{
            $this->view->params['desktop'] =  0;
        }
        $this->view->params['ads'] = Advertise::getAds();
        $this->view->params['config'] = Config::find()->one();
        $hn = HotNews::find()->one();
        $this->view->params['newest'] = News::find()
                                        ->where(["news_status" => News::status_published])
                                        ->andWhere(['not in','news_id',explode(",", $hn->special_news)])
                                        ->orderBy('news.published_at DESC')
                                        ->limit(6)->all();
        $this->view->params['top'] = News::find()
                                    ->where(['>=','created_at' , time() - 15*86400])
                                    ->andWhere(["news_status" => News::status_published])
                                    ->orderBy('news_viewed DESC')
                                    ->limit(15)->all();
        $this->enableCsrfValidation = false;
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($slug='')
    {
        $hn = HotNews::find()->one();
        return $this->render('index',[
            'page' => Menu::getMenuWithNews(3),
            'hot' => News::find()
                    ->where(['news_id' => $hn->hotest])
                    ->andWhere(["news_status" => News::status_published])->one(),
            'highlight' => News::find()
                    ->where('news_id IN ('.$hn->others.')')
                    ->andWhere(["news_status" => News::status_published])->all(),
            'new' => News::find()
                    ->where('news_id IN ('.$hn->special_news.')')
                    ->andWhere(["news_status" => News::status_published])->one()
        ]);
    }
    
    public function actionBaiViet($slug){
        $post = News::find()->where(['news_slug' => $slug])
                ->andWhere(["news_status" => News::status_published])->one();
        if(!$post) {
            return $this->redirect('/',302);
        }
        $post->news_viewed = $post->news_viewed + 1;
        $post->save();
        if($post->other_news)
            $post->other = News::find()->where(['news_id' => explode(',',$post->other_news)])->all();
        $post->news_viewed = $post->news_viewed + 1;
        $post->save();
        return $this->render('post',[
            'post' => $post,
            'sidemenu' => Menu::getSuggestMenu($post->menu_id),
            'recomend' => News::find()
                            ->leftJoin('cate_news', 'cate_news.news_id = news.news_id')
                            ->where(["news_status" => News::status_published])
                            ->andWhere(['cate_news.cate_id' => Helper::cateToArray($post->cateNews) ])
                            ->orderBy('news.published_at DESC')
                            ->limit(8)
                            ->all()
        ]);
    }
    public function actionTimKiem($keyword){
        $query = News::find()
                        ->where(["news_status" => News::status_published])
                        ->andWhere(['like', 'news_title', '%'.addslashes($keyword). '%', false])
                        ->orderBy('news_id DESC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('category', [
             'models' => $models,
             'sidemenu' => Menu::getSuggestMenu(2),
             'pages' => $pages,
             'title_cate' => 'Kết quả tìm kiếm: '.$keyword
        ]);
    }
    public function actionDanhMuc($slug)
    {
        $menu = Menu::find()->where(['menu_link' => $slug])->one();
        $menu->categories =  $menu->getCateMenu()->all();
        $list_cate = [];
        foreach($menu->categories as $cate){
            $list_cate[] = $cate->cate_id;
        }
        $query = News::find()->joinWith('cateNews')
                        ->where(["news_status" => News::status_published])
                        ->andWhere(['cate_news.cate_id' => $list_cate ])
                        ->orderBy('news.news_id DESC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('category', [
             'models' => $models,
             'sidemenu' => Menu::getSuggestMenu($menu->menu_id),
             'pages' => $pages,
             'title_cate' => 'Danh mục: '.$menu->menu_title
        ]);
    }
}