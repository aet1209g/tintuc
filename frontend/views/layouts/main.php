<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\Helper;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
    <?php $this->beginPage() ?>
        <!DOCTYPE html>
        <html lang=en xmlns:fb='http://www.facebook.com/2008/fbml'>

        <head>
            <meta charset="UTF-8" type="<?= $this->params['desktop'] ?>" />

            <?php if($this->params['desktop'] == 0):?>
                <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
                <?php endif;?>
                    <?php if (isset($this->blocks['meta_block'])): ?>
                        <?= $this->blocks['meta_block'] ?>
                            <?php else: ?>
                                <?php endif; ?>
                                    <meta property="og:site_name" content="Doanhnghiep.net.vn" />
                                    <meta property="fb:app_id" content="312766915775662" />
                                    <meta name="google-site-verification" content="tkFfAizQV8PfbRKa8FgC7rbGZsCUhiljmzcQrTQQSVU" />
                                    <link rel="icon" href="/images/logo/favicon.png">
                                    <!-- Latest compiled and minified CSS -->
                                    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">

                                    <?= Html::csrfMetaTags() ?>
                                        <title>
                                            <?= Html::encode($this->title) ?>
                                        </title>
                                        <link rel="stylesheet" href="/css/style.css">
                                        <script>
                                            window.fbAsyncInit = function () {
                                                FB.init({
                                                    appId: '312766915775662',
                                                    xfbml: true,
                                                    version: 'v2.8'
                                                });
                                            };

                                            (function (d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id)) {
                                                    return;
                                                }
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/sdk.js";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));
                                        </script>
                                        <script>
                                            (function (i, s, o, g, r, a, m) {
                                                i['GoogleAnalyticsObject'] = r;
                                                i[r] = i[r] || function () {
                                                    (i[r].q = i[r].q || []).push(arguments)
                                                }, i[r].l = 1 * new Date();
                                                a = s.createElement(o),
                                                    m = s.getElementsByTagName(o)[0];
                                                a.async = 1;
                                                a.src = g;
                                                m.parentNode.insertBefore(a, m)
                                            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                                            ga('create', 'UA-86816353-1', 'auto');
                                            ga('send', 'pageview');
                                        </script>
                                        <!--<meta property="fb:app_id" content="{YOUR_APP_ID}" />
            <meta property="fb:admins" content="{YOUR_FACEBOOK_USER_ID}"/>-->
        </head>

        <body class="">
            <?php $this->beginBody() ?>
                <header class="header">
                    <div class="container-page">
                        <div class="clearfix">
                            <div class="pull-left">
                                <a href="/">
                                    <img src="/images/logo/logo_white.png" class="logo" alt="logo doanh nghiep">
                                </a>
                            </div>
                            <div class="pull-left trends clear-fix">
                                <i class="trends-icon glyphicon glyphicon-send pull-left"></i>
                                <div class="swiper-container pull-right">
                                    <div class="swiper-wrapper">
                                        <?php foreach($this->params['newest'] as $key => $post_new):?>
                                            <div class="swiper-slide">
                                                <a href="<?= Helper::getPostUrl($post_new->news_slug) ?>" class="short_link"><?=$post_new->news_title ?></a>
                                            </div>
                                            <?php endforeach;?>
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <div class="search-box">
                                    <form class="form-inline clear-fix search-form" action="<?= Helper::getSearchUrl('') ?>">
                                        <input type="text" class="form-control pull-left" placeholder="Nhập từ khoá" name="keyword">
                                        <button type="submit" class="btn btn-primary pull-left">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <nav class="navbar navbar-default">
                    <div class="container-page">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/">
                                <img src="/images/logo/logo_white.png" alt="">
                            </a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="show-mobile search-box">
                                    <form class="form-inline search-form" action="<?= Helper::getSearchUrl('') ?>">
                                        <input type="text" class="form-control pull-left" placeholder="Nhập từ khoá" name="keyword">
                                        <button type="submit" class="btn btn-primary pull-right">Tìm kiếm</button>
                                    </form>
                                </li>
                                <?php foreach($this->params['menu'] as $menu):?>
                                    <li><a href="<?= Helper::getCategoryUrl($menu->menu_link) ?>" class="nav-items-link"><?= $menu->menu_title ?> </a></li>
                                    <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="container-page">
                    <main class="container-main">
                        <?=$content;?>
                    </main>
                </div>
                <footer>
                    <div class="top">
                        <div class="container-page">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <a href="/" class="short_link"><i class="glyphicon glyphicon-home"></i> Trang chủ</a>
                                </div>
                                <div class="pull-right">
                                    <a href="?mobile=true" class="short_link show-mobile">
                                        <img src="/images/desktop.svg" width=15 alt="">
                                    </a> &nbsp;&nbsp;
                                    <a href="#" class="short_link"> <img src="/images/top.svg" width=15 alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="center">
                        <div class="container-page">
                            <div class="row">
                                <?php foreach($this->params['menu'] as $menu):?>
                                    <?php if($menu->menu_id > 1):?>
                                        <div class="col-xs-4 col-sm-2">
                                            <ul class="list-group no-style">
                                                <li class="list-group-item">
                                                    <a rel="nofollow" href="<?= Helper::getCategoryUrl($menu->menu_link) ?>" class="short_link"><?= $menu->menu_title ?> </a>
                                                </li>
                                                <?php foreach($menu->childs as $sub_menu):?>
                                                    <li class="list-group-item">
                                                        <a rel="nofollow" href="<?= Helper::getCategoryUrl($sub_menu->menu_link) ?>" class="short_link"><?= $sub_menu->menu_title ?> </a>
                                                    </li>
                                                    <?php endforeach;?>
                                            </ul>
                                        </div>
                                        <?php endif;?>
                                            <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                    <div class="info clearfix">
                        <div class="container-page">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="/images/logo/logo_primary.png" width="100" alt="">
                                    <br>
                                    <br>
                                    <p><strong>&copy; 2016 All rights reserved</strong></p>
                                </div>
                                <div class="col-sm-9">
                                    <strong>Trang tin điện tử Doanh nghiệp Việt Nam. </strong>
                                    <br>
                                    <br>

                                    <span>Liên hệ: <?= $this->params['config']->email ?></span>

                                    <p>Phiên bản thử nghiệm, đang chờ xin phép cơ quan chức năng</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

                <script src="/js/jquery.min.js"></script>
                <script src="/js/swiper.js"></script>
                <script src="/bootstrap/js/bootstrap.min.js"></script>
                <?php $this->endBody() ?>
        </body>

        </html>
        <?php $this->endPage() ?>