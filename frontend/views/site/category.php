<?php
    use common\helpers\Helper;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    $this->title = $title_cate;
?>
    <?php $this->beginBlock('meta_block'); ?>
        <meta name="title" content="<?= $this->params['config']->title ?>" />
        <meta name="description" content="<?= $this->params['config']->desciprtion ?>" />
        <meta name="keywords" content="<?= $this->params['config']->keyword ?>" />
        <meta property="og:title" content="<?= $this->params['config']->title_social ?>" />
        <meta property="og:description" content="<?= $this->params['config']->description_social ?>" />
        <?php $this->endBlock(); ?>
            <section class="row">
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="content main">
                                <div class="info clearfix">
                                    <div class="pull-left">
                                        <h1 class="cate"><?= $title_cate ?></h1>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <?php if(count($models) > 0 ): ?>
                                <article class="post post-main">
                                    <div class="thumbnail">
                                        <a href="<?= Helper::getPostUrl($models[0]->news_slug) ?>" class="img-thumb">
                                            <div class="r43">
                                                <div class="r43-img">
                                                    <img src="<?= Helper::getPostImg($models[0]) ?>" alt="<?= $models[0]->news_title ?>">
                                                </div>
                                            </div>
                                        </a>
                                        <div class="caption">
                                            <a href="<?= Helper::getPostUrl($models[0]->news_slug) ?>">
                                                <h2><?= $models[0]->news_title ?></h2>
                                            </a>
                                            <p>
                                                <?= $models[0]->news_description ?>
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <div class="article">
                                    <?php foreach($models as $key => $post):?>
                                        <?php if($key > 0):?>
                                            <article class="post post-wide">
                                                <div class="thumbnail">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <a href="<?= Helper::getPostUrl($post->news_slug) ?>">
                                                                <div class="r43">
                                                                    <div class="r43-img">
                                                                        <img src="<?= Helper::getPostImg($post) ?>" alt="<?=$post->news_title ?>">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <div class="caption">
                                                                <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link regular"><?=$post->news_title ?></a>
                                                                <p>
                                                                    <?= $post->news_description  ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <?php endif; ?>
                                                <?php endforeach;?>
                                </div>
                                <?php echo LinkPager::widget([
                        'pagination' => $pages,
                    ]); ?>
                                    <?php else:?>
                                        <h3>Không tìm thấy kết quả nào</h3>
                                        <?php endif;?>
                        </div>
                        <div class="col-sm-3">
                            <ul class="list-group no-style sidebar">
                                <?php foreach($this->params['newest'] as $key => $post):?>
                                    <li class="list-group-item">
                                        <?php if($key == 0): ?>
                                            <div class="r43">
                                                <div class="r43-img">
                                                    <img src="<?= Helper::getPostImg($post) ?>" alt="<?= $post->news_title ?>">
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                                <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                    </li>
                                    <?php endforeach;?>
                            </ul>
                            <?php foreach($sidemenu as $key => $menu):?>
                                <div class="top-post">
                                    <div class="title-block">
                                        <a href="<?= Helper::getCategoryUrl($menu->menu_link) ?>"><h2><?= $menu->menu_title ?></h2></a>
                                    </div>
                                    <ul class="list-group no-style">
                                        <?php foreach($menu->posts as $key => $post):?>
                                            <li class="list-group-item">
                                                <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                            </li>
                                            <?php endforeach;?>
                                    </ul>
                                </div>
                                <?php endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 hidden-xs">
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->top_post->link ?>">
                        <img src="<?= $this->params['ads']->top_post->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->post_1->link ?>">
                        <img src="<?= $this->params['ads']->post_1->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="top-post suggest-block">
                        <div class="title-block">
                            <h2>BÀI ĐỌC NHIỀU NHẤT</h2>
                        </div>
                        <ul class="list-group no-style">
                            <?php foreach($this->params['top'] as $key => $post):?>
                                <li class="list-group-item">
                                    <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                </li>
                                <?php endforeach;?>
                        </ul>
                    </div>
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->post_2->link ?>">
                        <img src="<?= $this->params['ads']->post_2->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>

                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->post_3->link ?>">
                        <img src="<?= $this->params['ads']->post_3->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->slidebar_1->link ?>">
                        <img src="<?= $this->params['ads']->slidebar_1->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->slidebar_2->link ?>">
                        <img src="<?= $this->params['ads']->slidebar_2->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                </div>
            </section>