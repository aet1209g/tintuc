<?php

$this->title = $post->news_title;
use common\helpers\Helper;
use yii\helpers\Url;

?>
    <?php $this->beginBlock('meta_block'); ?>
        <meta name="title" content="<?= $post->news_title ?>" />
        <meta name="description" content="<?= $post->news_description ?>" />
        <meta name="keywords" content="Dieu Anh Biet, Chi Dan, , , lyrics, loi bai hat, chat luong cao" />
        <meta property="og:title" content="<?= $post->news_title ?> | Bài viết" />
        <meta property="og:description" content="<?= $post->news_description ?>" />
        <meta property="og:image" content="<?= Helper::getPostImg($post) ?>" />
        <meta property="og:url" content="<?= Helper::getPostUrl($post->news_slug,true) ?>" />
        <meta property="og:type" content="article" />
        <?php $this->endBlock(); ?>
            <section class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-sm-9">
                            <article class="content main">
                                <div class="info clearfix">
                                    <div class="pull-left">
                                        <span class="date">Xuất bản ngày: <?= date('d/m/Y h:i a',$post->published_at) ?></span>
                                    </div>
                                    <div class="pull-right">
                                        <div class="fb-like" data-href="<?= Helper::getPostUrl($post->news_slug,true) ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                    </div>
                                </div>
                                <h1><?= $post->news_title ?></h1>
                                <div class="fck_detail width_common block_ads_connect">
                                    <div class="post-description">
                                        <strong><?= str_replace("(Doanhnghiep.net.vn)","<span class='sitename'>Doanhnghiep.net.vn</span>",$post->news_description);  ?></strong>
                                    </div>
                                    <?php if($post->other): ?>
                                        <div class="other-news">
                                            <ul class="list-group">
                                                <?php  foreach($post->other as $key => $pos):?>
                                                    <li class="list-group-item">
                                                        <a href="<?= Helper::getPostUrl($pos->news_slug) ?>" class="short_link"><?=$pos->news_title ?></a>
                                                    </li>
                                                    <?php endforeach;?>
                                            </ul>
                                        </div>
                                        <?php  endif; ?>
                                            <div class="post--main">
                                                <?= $post->news_content ?>
                                            </div>
                                            <div class="post--author text-right">
                                                <strong><?= $post->author ?> <?= $post->source ? '('.$post->source.')' :'' ?> </strong>
                                            </div>
                                </div>
                            </article>
                            <div class="tag">
                                <br>
                                <br>
                                <h4>Từ khoá bài viết</h4>
                                <?php if(!count($post->tags)): ?>
                                    <span>Bài viết không được gắn tag nào</span>
                                    <?php endif; ?>
                                        <?php  foreach($post->tags as $key => $tag):?>
                                            <span class="tag"><?= $tag->detail->tag_title ?></span>
                                            <?php endforeach;?>
                            </div>
                        </div>
                        <div class="col-sm-3 hide-ads">
                            <ul class="list-group no-style sidebar">
                                <?php foreach($this->params['newest'] as $key => $pos):?>
                                    <li class="list-group-item">
                                        <?php if($key == 0): ?>
                                            <div class="r43">
                                                <div class="r43-img">
                                                    <img src="<?= Helper::getPostImg($pos) ?>" alt="<?= $pos->news_title ?>">
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                                <a href="<?= Helper::getPostUrl($pos->news_slug) ?>" class="short_link"><?=$pos->news_title ?></a>
                                    </li>
                                    <?php endforeach;?>
                            </ul>
                            <?php foreach($sidemenu as $key => $menu):?>
                                <div class="top-post">
                                    <div class="title-block">
                                        <a href="<?= Helper::getCategoryUrl($menu->menu_link) ?>"><h2><?= $menu->menu_title ?></h2></a>
                                    </div>
                                    <ul class="list-group no-style">
                                        <?php foreach($menu->posts as $key => $post):?>
                                            <li class="list-group-item">
                                                <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                            </li>
                                            <?php endforeach;?>
                                    </ul>
                                </div>
                                <?php endforeach;?>
                        </div>
                    </div>
                    <div class="extra">
                        <div class="comments">
                            <div class="top">
                                Ý kiến bạn đọc
                            </div>
                            <div class="content">
                                <div class="fb-comments" data-order-by="social" data-width="100%" data-href="<?= Helper::getPostUrl($post->news_slug,true) ?>" data-numposts="15"></div>
                            </div>
                        </div>

                        <div class="category">
                            <ul class="nav nav-tabs category-tabs">
                                <li class="active"><a href="#" class="short_link">TIN CÙNG CHUYÊN MỤC</a></li>
                            </ul>
                            <div class="article">
                                <div class="row">
                                    <?php foreach($recomend as $key => $pos):?>
                                        <div class="col-xs-6 col-sm-3">
                                            <article class="post post-small">
                                                <div class="thumbnail">
                                                    <a href="<?= Helper::getPostUrl($pos->news_slug) ?>">
                                                        <div class="r43">
                                                            <div class="r43-img">
                                                                <img src="<?= Helper::getPostImg($pos) ?>" alt="<?=$pos->news_title ?>">
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="caption">
                                                        <a href="<?= Helper::getPostUrl($pos->news_slug) ?>" class="short_link"><h4><?=$pos->news_title ?></h4></a>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 hide-ads">
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->top_post->link ?>">
                        <img src="<?= $this->params['ads']->top_post->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="ads block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->post_1->link ?>">
                        <img src="<?= $this->params['ads']->post_1->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="top-post suggest-block">
                        <div class="title-block">
                            <h2>BÀI ĐỌC NHIỀU NHẤT</h2>
                        </div>
                        <ul class="list-group no-style">
                            <?php foreach($this->params['top'] as $key => $pos):?>
                                <li class="list-group-item">
                                    <a href="<?= Helper::getPostUrl($pos->news_slug) ?>" class="short_link"><?=$pos->news_title ?></a>
                                </li>
                                <?php endforeach;?>
                        </ul>
                    </div>
                    <div class="ads block-ads">
                       <a rel="nofollow" href="<?= $this->params['ads']->post_2->link ?>">
                        <img src="<?= $this->params['ads']->post_2->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="ads block-ads">
                       <a rel="nofollow" href="<?= $this->params['ads']->post_3->link ?>">
                        <img src="<?= $this->params['ads']->post_3->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                </div>
            </section>