<?php
use common\helpers\Helper;
use yii\helpers\Url;
$this->title = $this->params['config']->title;
?>
    <?php $this->beginBlock('meta_block'); ?>
        <meta name="title" content="<?= $this->params['config']->title ?>" />
        <meta name="description" content="<?= $this->params['config']->desciprtion ?>" />
        <meta name="keywords" content="<?= $this->params['config']->keyword ?>" />
        <meta property="og:title" content="<?= $this->params['config']->title_social ?>" />
        <meta property="og:description" content="<?= $this->params['config']->description_social ?>" />
        <?php $this->endBlock(); ?>
            <section class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-9">
                            <article class="post post-main">
                                <div class="thumbnail">
                                    <a href="<?= Helper::getPostUrl($hot->news_slug) ?>" class="img-thumb">
                                        <div class="r43">
                                            <div class="r43-img">
                                                <img src="<?= Helper::getPostImg($hot) ?>" alt="<?= $hot->news_title ?>">
                                            </div>
                                        </div>
                                    </a>
                                    <div class="caption">
                                        <a href="<?= Helper::getPostUrl($hot->news_slug) ?>">
                                            <h1><?= $hot->news_title ?></h1>
                                        </a>
                                        <p>
                                            <?= $hot->news_description ?>
                                        </p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-3">
                            <ul class="list-group no-style sidebar index-side">
                                <li class="list-group-item">
                                    <div class="r43">
                                        <div class="r43-img">
                                            <img src="<?= Helper::getPostImg($new) ?>" alt="<?= $new->news_title ?>">
                                        </div>
                                    </div>
                                    <a href="<?= Helper::getPostUrl($new->news_slug) ?>" class="short_link"><?=$new->news_title ?></a>
                                </li>
                                <?php foreach($this->params['newest'] as $key => $post):?>
                                    <li class="list-group-item">
                                        <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                    </li>
                                    <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach($highlight as $key => $post):?>
                            <div class="col-xs-6 col-md-3">
                                <article class="post post-small">
                                    <div class="thumbnail">
                                        <a href="<?= Helper::getPostUrl($post->news_slug) ?>">
                                            <div class="r43">
                                                <div class="r43-img">
                                                    <img src="<?= Helper::getPostImg($post) ?>" alt="<?=$post->news_title ?>">
                                                </div>
                                            </div>
                                        </a>
                                        <div class="caption">
                                            <a class="short_link" href="<?= Helper::getPostUrl($post->news_slug) ?>">
                                                <h4><?=$post->news_title ?></h4>
                                            </a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <?php endforeach;?>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->banner_1->link ?>">
                        <img src="<?= $this->params['ads']->banner_1->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="block-ads">
                        <a rel="nofollow" href="<?= $this->params['ads']->top_index->link ?>">
                        <img src="<?= $this->params['ads']->top_index->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="fb-page" data-href="<?= $this->params['config']->facebook_page ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
                        <blockquote cite="<?= $this->params['config']->facebook_page ?>" class="fb-xfbml-parse-ignore"><a href="<?= $this->params['config']->facebook_page ?>">Doanhnghiep.net.vn</a></blockquote>
                    </div>
                    <br>
                    <br>
                </div>
            </section>
            <section class="row">
                <div class="col-md-9">
                    <?php foreach($page as $key => $block):?>
                        <?php if($block->menu_id > 1 && $key < count($page) - 3):?>
                            <div class="category">
                                <ul class="nav nav-tabs category-tabs">
                                    <li class="active">
                                        <a href="<?= Helper::getCategoryUrl($block->menu_link) ?>" class="short_link"><h3><?= $block->menu_title ?></h3></a>
                                    </li>
                                    <?php foreach($block->childs as $key => $sub_menu):?>
                                        <?php if($key < 5):?>
                                            <li>
                                                <a href="<?= Helper::getCategoryUrl($sub_menu->menu_link) ?>" class="short_link"><?= $sub_menu->menu_title ?> </a>
                                            </li>
                                            <?php endif; ?>
                                                <?php endforeach;?>
                                </ul>
                                <div class="article">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-8">
                                            <article class="post post-wide">
                                                <div class="thumbnail">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <a href="<?= Helper::getPostUrl($block->highlight->news_slug) ?>">
                                                                <div class="r43">
                                                                    <div class="r43-img">
                                                                        <img src="<?= Helper::getPostImg($block->highlight) ?>" alt="<?=$block->highlight->news_title ?>">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <div class="caption">
                                                                <a href="<?= Helper::getPostUrl($block->highlight->news_slug) ?>" class="short_link"><?=$block->highlight->news_title ?></a>
                                                                <p>
                                                                    <?= $block->highlight->news_description  ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <ul class="list-group">
                                                <?php foreach($block->posts as $key => $post):?>
                                                    <li class="list-group-item">
                                                        <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                                    </li>
                                                    <?php endforeach;?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                                <?php endforeach;?>
                                    <div class="row">
                                        <?php foreach($page as $key => $block):?>
                                            <?php if($key >= count($page) - 3):?>
                                                <div class="col-sm-4">
                                                    <div class="top-post suggest-block">
                                                        <div class="title-block">
                                                            <a href="<?= Helper::getCategoryUrl($block->menu_link) ?>"><h4><?= $block->menu_title ?></h4></a>
                                                        </div>
                                                        <ul class="list-group no-style">
                                                            <li class="list-group-item">
                                                                <div class="r43">
                                                                    <div class="r43-img">
                                                                        <img src="<?= Helper::getPostImg($block->highlight) ?>" alt="<?=$block->highlight->news_title ?>">
                                                                    </div>
                                                                </div>
                                                                <a href="<?= Helper::getPostUrl($block->highlight->news_slug) ?>" class="short_link bold"><?=$block->highlight->news_title ?></a>
                                                            </li>
                                                            <?php foreach($block->posts as $key => $post):?>
                                                                <?php if($key > 0):?>
                                                                    <li class="list-group-item">
                                                                        <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                                                    </li>
                                                                    <?php endif; ?>
                                                                        <?php endforeach;?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                    <?php endforeach;?>
                                    </div>
                </div>
                <div class="col-md-3 hide-ads">
                    <div class="top-post suggest-block">
                        <div class="title-block">
                            <h2>BÀI ĐỌC NHIỀU NHẤT</h2>
                        </div>
                        <ul class="list-group no-style">
                            <?php foreach($this->params['top'] as $key => $post):?>
                                <li class="list-group-item">
                                    <a href="<?= Helper::getPostUrl($post->news_slug) ?>" class="short_link"><?=$post->news_title ?></a>
                                </li>
                                <?php endforeach;?>
                        </ul>
                    </div>
                    <div class="ads block-ads">
                       <a rel="nofollow" href="<?= $this->params['ads']->slidebar_1->link ?>">
                        <img src="<?= $this->params['ads']->slidebar_1->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                    <div class="ads block-ads">
                       <a rel="nofollow" href="<?= $this->params['ads']->slidebar_2->link ?>">
                        <img src="<?= $this->params['ads']->slidebar_2->image ?>" class="hide-ads" alt="">
                        </a>
                    </div>
                </div>
            </section>
            <div class="ads-bottom">
                <div class="block-ads">
                   <a rel="nofollow" href="<?= $this->params['ads']->footer->link ?>">
                    <img src="<?= $this->params['ads']->footer->image ?>" alt="">
                    </a>
                </div>
            </div>